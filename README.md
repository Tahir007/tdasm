# TDasm - x64 JIT assembler for Python #

### About ###

TDasm is x64 dynamic JIT assembler for Python. It generate native code for x64 architecture and supports
MMX, FPU, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.1, RDRAND, AVX, AVX2 and AVX512 instruction sets. For writing
assembly code intel syntax is used. 64-bit Python is required to use TDasm.

TODOs:

1. Some general instructions are still missing
2. AVX512 support is not complete yet
3. Documentation


### Example how to use hardware random generator ###

``` python
from tdasm import Runtime, translate

code = """
    #DATA
    uint32 rnd_num
    #CODE
    _gen_rnd:
    rdrand eax
    jnc _gen_rnd
    mov dword [rnd_num], eax
"""

mc = translate(code)
run = Runtime()
ds = run.load("generate_rnd", mc)
run.run('generate_rnd')
print(ds['rnd_num'])
run.run('generate_rnd')
print(ds['rnd_num'])
```

### Memcopy routine for copying arrays in python. ###

``` python
from tdasm import translate, Runtime
from array import array


_asm_code = """
    #DATA
    uint64 sa, da, n

    #CODE
    mov rcx, qword [n]
    mov rsi, qword [sa]
    mov rdi, qword [da]
    rep movsb
"""

_runtime = Runtime()
_data = _runtime.load("memcopy", translate(_asm_code))


def memcopy(da, sa, n):
    """Copy n bytes form source address(sa) to destination address(da)."""
    _data["da"] = da
    _data["sa"] = sa
    _data["n"] = n
    _runtime.run("memcopy")


if __name__ == '__main__':

    arr = array('f')
    arr.append(5)
    arr.append(9)

    arr2 = array('f')
    arr2.append(59)
    arr2.append(79)

    sa, nelem = arr.buffer_info()
    da, nelem2 = arr2.buffer_info()

    memcopy(da, sa, 8)
    print(arr)
    print(arr2)
```
