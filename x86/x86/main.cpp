#ifdef MICROSOFT_WINDOWS
#include<windows.h>
#endif

#include <Python.h>
#include <structmember.h>
#include<stdio.h>
#include<stdlib.h>

#ifndef MICROSOFT_WINDOWS
#include<unistd.h>
#include<malloc.h>
#include<sys/mman.h>
#endif

#define ALIGN2( len ) (( len + 1 ) & ~1 )    // round up to 16 bits
#define ALIGN4( len ) (( len + 3 ) & ~3 )    // round up to 32 bits
#define ALIGN8( len ) (( len + 7 ) & ~7 )    // round up to 64 bits
#define ALIGN16( len ) (( len + 15 ) & ~15 ) // round up to 128 bits
#define ALIGN32( len ) (( len + 31 ) & ~31 ) // round up to 256 bits
#define ALIGN64( len ) (( len + 63 ) & ~63 ) // round up to 256 bits
typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef uint64 _uint;
#define Addr_f "K"
#define Addr_sz_f "KI"
#define Addr_idx_sz_f "KII"
#define Addr_py_idx_f "KOI"

typedef struct RamHeadType
{
  uint32 nReqSize; // Requested size
  uint32 extra[15]; // padding to help align to 64 byte.
} RamHead;

void * ramAlloc( uint32 nReqSize )
{
  void *pMem;
  RamHead *pHead;
  uint32 nSize;

  // Force to 64-byte block + room for header
  nSize = ALIGN64(nReqSize) + sizeof(RamHead);
  pHead = (RamHead *) malloc(nSize);
  pMem = pHead;
  if (NULL==pHead)
  {                     // Allocation Error
  }
  else
  {                     // Save Req Size
    pHead->nReqSize = nReqSize + sizeof(RamHead);
    pHead->extra[0] = 1;
    pHead->extra[1] = 2;
    pHead->extra[2] = 3;
    pHead->extra[3] = 4;
    pHead->extra[4] = 5;
    pHead->extra[5] = 6;
    pHead->extra[6] = 7;
    pHead->extra[7] = 8;
    pHead->extra[8] = 9;
    pHead->extra[9] = 10;
    pHead->extra[10] = 11;
    pHead->extra[11] = 12;
    pHead->extra[12] = 13;
    pHead->extra[13] = 14;
    pHead->extra[14] = 15;
      // Align by adj header +4 to +64 bytes
    pMem = (void *) ALIGN64( ((_uint)pMem) + sizeof(uint32) );
  }
  return pMem;
}

void ramFree( const void * const pRaw )
{

  uint32 *pMem;
  pMem = (uint32 *)pRaw;

  if (*(--pMem)<sizeof(RamHead))
  {
    pMem -= *pMem;
  }
    // pMem original (unadjusted) pointer
  free(pMem);
}

bool ramGet(void ** const ppMem, uint32 nReqSize )
{
  *ppMem = ramAlloc(nReqSize);
  return (NULL!=*ppMem) ? true : false;
}

bool ramGetClr(void **const ppMem, uint32 nReqSize)
{
  bool ret;
  ret = false;
  *ppMem = ramAlloc(nReqSize);
  if (NULL!=*ppMem)
  {
    memset(*ppMem, 0, nReqSize);
    ret = true;
  }
  return ret;
}

void ramRelease(void ** const ppMem )
{
  ramFree(*ppMem);
  *ppMem = NULL;
}

static PyObject *
GetData(PyObject *self, PyObject *args)
{
	unsigned int size;
	_uint address;
	if (! PyArg_ParseTuple(args, Addr_sz_f, &address, &size))
		return NULL;
	else
	{
		return Py_BuildValue("y#", (char*)address, size);
	}
}

static PyObject *
SetData(PyObject *self, PyObject *args)
{
	uint32 size;
	_uint address;
	char *data;
	char *dest;

	if (!PyArg_ParseTuple(args, "Ky#", &address, &data, &size))
		return NULL;
	else
	{
		dest = (char*) address;
		for(uint32 n=0; n<size; n++)
		{
			dest[n] = data[n];
		}
		Py_RETURN_NONE;
	}
}

static PyObject *
ExecuteModule(PyObject *self, PyObject *args)
{
	_uint address;
	if (!PyArg_ParseTuple(args, Addr_f, &address))
		return NULL;
	else
	{
		// Cast _code to the pointer to a function returning void
		void (*p)() = (void (*)())address;
		p();
		Py_RETURN_NONE;
	}
}

#ifdef MICROSOFT_WINDOWS
#define MAX_THREADS 128
DWORD WINAPI ThreadMain(LPVOID lp)
{
	void (*p)() = (void (*)())lp;
	p();
	return 0;
}

static PyObject *
ExecuteModules(PyObject *self, PyObject *args)
{
	HANDLE hThr[MAX_THREADS];
	_uint address;

	PyObject *py;
	if (!PyArg_ParseTuple(args, "O", &py))
			return NULL;
	else
	{
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			if (tup_size > MAX_THREADS) Py_RETURN_NONE;

			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				address = (unsigned long long)  PyLong_AsUnsignedLongLongMask(item);
				hThr[i] = CreateThread(NULL, 1048576,(LPTHREAD_START_ROUTINE)ThreadMain,(LPVOID)address, 0, NULL);
			}
			WaitForMultipleObjects(tup_size,hThr,TRUE,INFINITE);

		}
		Py_RETURN_NONE;
	}
}

static PyObject *
RunThreads(PyObject *self, PyObject *args)
{
	static HANDLE hThr[MAX_THREADS];
	static uint64 thread_addrs[MAX_THREADS];
	static Py_ssize_t tup_size = 0;
	_uint address;


	PyObject *py;
	if (!PyArg_ParseTuple(args, "O", &py))
			return NULL;
	else
	{
		if (PyTuple_Check(py))
		{
			tup_size = PyTuple_GET_SIZE(py);
			if (tup_size > MAX_THREADS) Py_RETURN_NONE;

			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				address = (unsigned long long)  PyLong_AsUnsignedLongLongMask(item);
				thread_addrs[i] = address;
				hThr[i] = CreateThread(NULL, 1048576,(LPTHREAD_START_ROUTINE)ThreadMain,(LPVOID)address, 0, NULL);
			}
			int iArrayIndex = WaitForMultipleObjects(tup_size,hThr,FALSE,INFINITE);
			CloseHandle(hThr[iArrayIndex]);
			address = thread_addrs[iArrayIndex];
			hThr[iArrayIndex] = hThr[tup_size - 1];
			thread_addrs[iArrayIndex] = thread_addrs[tup_size - 1];
			tup_size -= 1;
			return PyLong_FromUnsignedLongLong(address);
		}
		address = (unsigned long long)  PyLong_AsUnsignedLongLongMask(py);
		if (address == 0)
		{
			if (tup_size == 0)
				Py_RETURN_NONE;
			WaitForMultipleObjects(tup_size,hThr,TRUE,INFINITE);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				CloseHandle(hThr[i]);
			}
			Py_RETURN_NONE;
		}
		else
		{

			hThr[tup_size] = CreateThread(NULL, 1048576,(LPTHREAD_START_ROUTINE)ThreadMain,(LPVOID)address, 0, NULL);
			thread_addrs[tup_size] = address;
			int iArrayIndex = WaitForMultipleObjects(tup_size+1,hThr,FALSE,INFINITE);
			CloseHandle(hThr[iArrayIndex]);
			address = thread_addrs[iArrayIndex];
			hThr[iArrayIndex] = hThr[tup_size];
			thread_addrs[iArrayIndex] = thread_addrs[tup_size];
			return PyLong_FromUnsignedLongLong(address);
		}

		Py_RETURN_NONE;
	}
}
#else
#include <thread>
static const int num_threads = 128;

int ThreadMain(unsigned long long address)
{
	void (*p)() = (void (*)())address;
	p();
	return 0;
}

static PyObject *
ExecuteModules(PyObject *self, PyObject *args)
{
	std::thread hThr[num_threads];
	_uint address;

	PyObject *py;
	if (!PyArg_ParseTuple(args, "O", &py))
			return NULL;
	else
	{
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			if (tup_size > num_threads) Py_RETURN_NONE;

			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				address = (unsigned long long)  PyLong_AsUnsignedLongLongMask(item);
				hThr[i] = std::thread(ThreadMain, address);
			}
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				hThr[i].join();
			}
		}
		Py_RETURN_NONE;
	}
}
#endif

static PyObject *
GetInt64(PyObject *self, PyObject *args)
{
	// num is number of elements in array
	_uint address;
	uint32 index, num;
	long long *k;
	long long n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;

	if (num == 0)
		return PyLong_FromLongLong(*(long long*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (long long *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromLongLong(n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject *
GetInt32(PyObject *self, PyObject *args)
{
	// num is number of elements in array
	_uint address;
	uint32 index, num;
	int *k;
	int n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;

	if (num == 0)
		return PyLong_FromLong(*(int*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (int *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromLong(n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject *
GetInt16(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	short int *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;
	if (num == 0)
		return PyLong_FromLong((long)*(short*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (short int *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromLong((long)n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}
static PyObject *
GetInt8(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	char *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;
	if (num == 0)
		return PyLong_FromLong((long)*(char*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (char *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromLong((long)n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}

}

static PyObject*
GetUInt64(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	unsigned long long *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;

	if (num == 0)
		return PyLong_FromUnsignedLongLong(*(unsigned long long*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (unsigned long long*) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromUnsignedLongLong(n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject*
GetUInt32(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	uint32 *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;

	if (num == 0)
		return PyLong_FromUnsignedLong(*(unsigned int*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (uint32 *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromUnsignedLong(n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject*
GetUInt16(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	unsigned short *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;
	if (num == 0)
		return PyLong_FromLong((long)*(unsigned short*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (unsigned short *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromLong((long)n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject*
GetUInt8(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	unsigned char *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;
	if (num == 0)
		return PyLong_FromLong((long)*(unsigned char*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (unsigned char *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyLong_FromLong((long)n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject*
GetFloat(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	float *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;
	if (num == 0)
	{
		float real = *(float*)address;
		return PyFloat_FromDouble((double)real);
	}
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (float *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyFloat_FromDouble((double)n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject*
GetDouble(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index, num;
	double *k, n;

	if (! PyArg_ParseTuple(args, Addr_idx_sz_f, &address, &index, &num))
		return NULL;
	if (num == 0)
		return PyFloat_FromDouble(*(double*)address);
	else
	{
		PyObject *p = PyTuple_New((Py_ssize_t)(num));
		k = (double *) address;

		for (uint32 i=0; i<num; i++)
		{
			n = k[index + i];
			PyObject *m = PyFloat_FromDouble((double)n);
			PyTuple_SET_ITEM(p, i, m);
		}
		return p;
	}
}

static PyObject*
SetInt64(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	long long *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (long long*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (long long) PyLong_AsLongLong(item);
			}
		}
		else
		{
			a[index] =(long long) PyLong_AsLongLong(py);
		}
		Py_RETURN_NONE;
	}
}


static PyObject*
SetInt32(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	int *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (int*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (int) PyLong_AsLong(item);
			}
		}
		else
		{
			a[index] =(int) PyLong_AsLong(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetInt16(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	short int *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (short int*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (short int) PyLong_AsLong(item);
			}
		}
		else
		{
			a[index] =(short int) PyLong_AsLong(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetInt8(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	char *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (char*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (char) PyLong_AsLong(item);
			}
		}
		else
		{
			a[index] =(char) PyLong_AsLong(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetUInt64(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	unsigned long long *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (unsigned long long*)address;
		if (PyTuple_Check(py))
		{

			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (unsigned long long)  PyLong_AsUnsignedLongLongMask(item);
			}
		}
		else
		{
			a[index] =(unsigned long long) PyLong_AsUnsignedLongLongMask(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetUInt32(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	unsigned int *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (unsigned int*)address;
		if (PyTuple_Check(py))
		{

			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (unsigned int)  PyLong_AsUnsignedLongMask(item);
			}
		}
		else
		{
			a[index] =(unsigned int) PyLong_AsUnsignedLongMask(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetUInt16(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	unsigned short *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (unsigned short*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (unsigned short) PyLong_AsLong(item);
			}
		}
		else
		{
			a[index] =(unsigned short) PyLong_AsLong(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetUInt8(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	unsigned char *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (unsigned char*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (unsigned char) PyLong_AsLong(item);
			}
		}
		else
		{
			a[index] =(unsigned char) PyLong_AsLong(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetFloat(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	float *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (float*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (float)PyFloat_AsDouble(item);
			}
		}
		else
		{
			a[index] = (float)PyFloat_AsDouble(py);
		}
		Py_RETURN_NONE;
	}
}

static PyObject*
SetDouble(PyObject *self, PyObject *args)
{
	_uint address;
	uint32 index;
	PyObject *py;
	double *a;

	if (!PyArg_ParseTuple(args, Addr_py_idx_f, &address, &py, &index))
		return NULL;
	else
	{
		a = (double*)address;
		if (PyTuple_Check(py))
		{
			Py_ssize_t tup_size = PyTuple_GET_SIZE(py);
			for (Py_ssize_t i=0; i<tup_size; i++)
			{
				PyObject* item = PyTuple_GET_ITEM(py, i);
				a[index + i] = (double)PyFloat_AsDouble(item);
			}
		}
		else
		{
			a[index] = (double)PyFloat_AsDouble(py);
		}
		Py_RETURN_NONE;
	}
}




// implementation of python object for memory allocation
typedef struct {
  PyObject_HEAD
  void *ptr;
  Py_ssize_t size;
} MemObject;

static PyObject *
mem_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
  Py_ssize_t size;

  MemObject *self = (MemObject *) type->tp_alloc(type, 0);
  if (!self)
    return NULL;
  if (!PyArg_ParseTuple(args, "n", &size)) {
    //Py_DECREF(self); // FIXME
    return NULL;
  }

  bool result;
  result = ramGet(&self->ptr, size);
  if (!result)
  {
    Py_DECREF(self);
    return PyErr_SetFromErrno(PyExc_MemoryError);
  }
  self->size = size;
  return (PyObject *) self;
}

static void
mem_dealloc(MemObject *self)
{
  ramRelease(&self->ptr);
  self->ptr = NULL;
  //PyObject_DEL((PyObject*)self);
  //self->ob_base->ob_type->tp_free((PyObject*)self);
  self->ob_base.ob_type->tp_free((PyObject*)self);
}

static PyObject *
mem_ptr(MemObject *self)
{
  return PyLong_FromVoidPtr(self->ptr);
}

static PyObject *
mem_fill(MemObject *self)
{
  memset(self->ptr, 0, self->size);
  Py_RETURN_NONE;
}

static PyMethodDef mem_methods[] = {
  {"ptr", (PyCFunction) mem_ptr, METH_NOARGS },
  {"fill", (PyCFunction) mem_fill, METH_NOARGS },
  {NULL}
};

static PyTypeObject MemDataType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "x86.MemData",             /*tp_name*/
    sizeof(MemObject),	       /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor) mem_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT,        /*tp_flags*/
    0,                         /*tp_doc */
    0,                         /*tp_traverse */
    0,                         /*tp_clear*/
    0,                         /*tp_richcompare*/
    0,                         /*tp_weaklistoffset*/
    0,                         /*tp_iter*/
    0,                         /*tp_iternext*/
    mem_methods,               /*tp_methods*/
    0,                         /*tp_members*/
    0,                         /*tp_getset*/
    0,                         /*tp_base*/
    0,                         /*tp_dict*/
    0,                         /*tp_descr_get*/
    0,                         /*tp_descr_set*/
    0,                         /*tp_dictoffset*/
    0,                         /*tp_init*/
    0,                         /*tp_alloc*/
    mem_new,                   /*tp_new*/
};

void *allocate_exe_memory(unsigned int nPages)
{

    #ifdef MICROSOFT_WINDOWS
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	DWORD region_size = si.dwAllocationGranularity; //64K was hard coded in the past?
	region_size = 65536; // FIXME
	// Allocate the pages that can be read, written to, and executed
    #else
    unsigned int region_size = 65536;
    #endif

        #ifdef MICROSOFT_WINDOWS
		return  (void *)VirtualAlloc(NULL, region_size * (nPages), MEM_COMMIT,
                             PAGE_EXECUTE_READWRITE);

        #else
	void *pMem;
        //ramGet(&pMem, region_size*nPages);
	long pagesize = sysconf(_SC_PAGE_SIZE);
	pMem = memalign(pagesize, region_size*nPages);
	if (mprotect(pMem, region_size*nPages, PROT_READ | PROT_WRITE | PROT_EXEC))
		return NULL;
	return pMem;

        #endif
}
static PyObject *
mem_exe_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
  Py_ssize_t size;

  MemObject *self = (MemObject *) type->tp_alloc(type, 0);
  if (!self)
    return NULL;
  if (!PyArg_ParseTuple(args, "n", &size)) {
    Py_DECREF(self);
    return NULL;
  }
  self->ptr = allocate_exe_memory(size);
  if (self->ptr == NULL)
  {
    Py_DECREF(self);
    return PyErr_SetFromErrno(PyExc_MemoryError);
  }
  self->size = size;
  return (PyObject *) self;
}

static void
mem_exe_dealloc(MemObject *self)
{
	#ifdef MICROSOFT_WINDOWS
		LPVOID lpaddr = (LPVOID) self->ptr;
		VirtualFree(lpaddr, 0, MEM_RELEASE);
	#else
	void *pMem;
	pMem = (void *) self->ptr;
	//ramRelease(&pMem);
	free(pMem);

	#endif
  self->ptr = NULL;
  //PyObject_DEL(self); //istraziti ovaj slucaj
  self->ob_base.ob_type->tp_free((PyObject*)self);
}

static PyTypeObject MemExeType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "x86.MemExe",             /*tp_name*/
    sizeof(MemObject),	       /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor) mem_exe_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT,        /*tp_flags*/
    0,                         /*tp_doc */
    0,                         /*tp_traverse */
    0,                         /*tp_clear*/
    0,                         /*tp_richcompare*/
    0,                         /*tp_weaklistoffset*/
    0,                         /*tp_iter*/
    0,                         /*tp_iternext*/
    mem_methods,               /*tp_methods*/
    0,                         /*tp_members*/
    0,                         /*tp_getset*/
    0,                         /*tp_base*/
    0,                         /*tp_dict*/
    0,                         /*tp_descr_get*/
    0,                         /*tp_descr_set*/
    0,                         /*tp_dictoffset*/
    0,                         /*tp_init*/
    0,                         /*tp_alloc*/
    mem_exe_new,                   /*tp_new*/
};

/* registration table  */
static struct PyMethodDef x86_methods[] = {
	{"GetData", GetData, METH_VARARGS, " Get raw data from the specified address. "},
	{"SetData", SetData, METH_VARARGS, "Set raw data to specified address. "},
	{"ExecuteModule", ExecuteModule, METH_VARARGS, "Execute module at specified address. "},
	{"ExecuteModules", ExecuteModules, METH_VARARGS, "Execute multiple modules at the same time and wait until all modules are finished executing."},
#ifdef MICROSOFT_WINDOWS
	{"RunThreads", RunThreads, METH_VARARGS, "Run multiple modules at specified addresses(address)."},
#endif
	{"GetInt64", GetInt64, METH_VARARGS, "Get int64 from specified address."},
	{"GetInt32", GetInt32, METH_VARARGS, "Get int from specified address."},
	{"GetInt16", GetInt16, METH_VARARGS, "Get short form specified address."},
	{"GetInt8", GetInt8, METH_VARARGS, "Get byte from specified address."},
	{"GetUInt64", GetUInt64, METH_VARARGS, "Get uint64 frome specified address."},
	{"GetUInt32", GetUInt32, METH_VARARGS, "Get unsigned int frome specified address."},
	{"GetUInt16", GetUInt16, METH_VARARGS, "Get Unsigned short frome specified address."},
	{"GetUInt8", GetUInt8, METH_VARARGS, "Get unsigned byte from specified address."},
	{"GetFloat", GetFloat, METH_VARARGS, "Get float from specified address."},
	{"GetDouble", GetDouble, METH_VARARGS, "Get double from specified address"},
	{"SetInt64", SetInt64, METH_VARARGS, "Set Int64 at specified address."},
	{"SetInt32", SetInt32, METH_VARARGS, "Set Int32 at specified address."},
	{"SetInt16", SetInt16, METH_VARARGS, "Set Int16 at specified address."},
	{"SetInt8", SetInt8, METH_VARARGS, "Set Int8 at specified address."},
	{"SetUInt64", SetUInt64, METH_VARARGS, "Set UInt64 at specified address."},
	{"SetUInt32", SetUInt32, METH_VARARGS, "Set UInt32 at specified address."},
	{"SetUInt16", SetUInt16, METH_VARARGS, "Set UInt16 at specified address."},
	{"SetUInt8", SetUInt8, METH_VARARGS, "Set UInt8 at specified address."},
	{"SetFloat", SetFloat, METH_VARARGS, "Set Float at specified address."},
	{"SetDouble", SetDouble, METH_VARARGS, "Set Double at specified address."},
    {NULL, NULL, 0, NULL}                   /* end of table marker */
};

static struct PyModuleDef x86module = {
   PyModuleDef_HEAD_INIT,
   "x86",   /* name of module */
   NULL, /* module documentation, may be NULL */
   -1,       /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
   x86_methods
};

/* module initializer */
PyMODINIT_FUNC PyInit_x86( )                       /* called on first import */
{                                      /* name matters if loaded dynamically */

	PyObject *m;

	if (PyType_Ready(&MemDataType) < 0)
    return NULL;

	if (PyType_Ready(&MemExeType) < 0)
    return NULL;
	m = PyModule_Create(&x86module);

	Py_INCREF(&MemExeType);
	PyModule_AddObject(m, "MemExe", (PyObject *)&MemExeType);

	Py_INCREF(&MemDataType);
	PyModule_AddObject(m, "MemData", (PyObject *)&MemDataType);
	return m;
}
