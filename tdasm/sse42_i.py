
PCMPESTRI = {
    "xmmxmmimm8": {"mod": "/r", "opcode": "660F3A61", "imm": "ib"},
    "xmmmem128imm8": {"mod": "/r", "opcode": "660F3A61", "imm": "ib"},
}

PCMPESTRM = {
    "xmmxmmimm8": {"mod": "/r", "opcode": "660F3A60", "imm": "ib"},
    "xmmmem128imm8": {"mod": "/r", "opcode": "660F3A60", "imm": "ib"},
}

PCMPISTRI = {
    "xmmxmmimm8": {"mod": "/r", "opcode": "660F3A63", "imm": "ib"},
    "xmmmem128imm8": {"mod": "/r", "opcode": "660F3A63", "imm": "ib"},
}

PCMPISTRM = {
    "xmmxmmimm8": {"mod": "/r", "opcode": "660F3A62", "imm": "ib"},
    "xmmmem128imm8": {"mod": "/r", "opcode": "660F3A62", "imm": "ib"},
}

PCMPGTQ = {
    "xmmxmm": {"mod": "/r", "opcode": "660F3837"},
    "xmmmem128": {"mod": "/r", "opcode": "660F3837"},
}
