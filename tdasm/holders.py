from typing import TypeVar, Sequence, Union


class Directive:
    def __init__(self, name: str):
        self.name = name

    def to_str(self):
        return '#%s\n' % self.name


class Label:
    def __init__(self, name: str):
        self.name = name

    def to_str(self):
        return '%s:\n' % self.name


class Keyword:
    def __init__(self, name: str, iden: str = None):
        self.name = name
        self.iden = iden

    def to_str(self):
        if self.iden:
            return '%s %s\n' % (self.name, self.iden)
        else:
            return '%s\n' % self.name


class DataMember:
    def __init__(self, name: str, typ: str, value=None):
        self.name = name
        self.typ = typ
        self.value = value

    def to_str(self):
        if self.value:
            return '%s %s = %s\n' % (self.typ, self.name, str(self.value))
        else:
            return '%s %s\n' % (self.typ, self.name)


class ArrayMember:
    def __init__(self, name: str, typ: str, length: int, values: list = None):
        self.name = name
        self.typ = typ
        self.length = length
        self.values = values

    def __len__(self):
        return self.length

    def to_str(self):
        if self.values:
            vals = ','.join(str(v) for v in self.values)
            return '%s %s[%i] = %s\n' % (self.typ, self.name, self.length, vals)
        else:
            return '%s %s[%i]\n' % (self.typ, self.name, self.length)


class StructDefinition:
    def __init__(self, name: str):
        self.name = name
        self._members = {}
        self.members = []

    def add_members(self, members: Sequence[Union[DataMember, ArrayMember]]):
        for member in members:
            if member.name in self._members:
                raise ValueError('Member %s allready exist.' % member.name)
            self._members[member.name] = member
            self.members.append(member)

    def to_str(self):
        start = 'struct %s\n' % self.name
        body = ''.join(m.to_str() for m in self.members)
        end = 'end\n'
        return start + body + end


class DataMembers:
    def __init__(self, members: Sequence[Union[DataMember, ArrayMember]]):
        self.members = members

    def __iter__(self):
        for member in self.members:
            yield member

    def to_str(self):
        return ''.join(m.to_str() for m in self.members)


class RegOperand:
    def __init__(self, reg: str):
        self.reg = reg
        self.op_mask = None
        self.op_zero = None

    def to_str(self):
        return self.reg


class MemOperand:
    def __init__(self, reg: str, data_type: str, scaled_reg: str = None,
                 scale: int = None, displacement: int = None, struct_member: str = None):
        self.reg = reg
        self.data_type = data_type
        self.scaled_reg = scaled_reg
        self.scale = scale
        self.displacement = displacement
        self.struct_member = struct_member
        self.op_mask = None
        self.op_zero = None

    def to_str(self):
        scale = ''
        if self.scaled_reg:
            scale = ' + %s' % self.scaled_reg
        if self.scale:
            scale += '*%i' % self.scale

        disp = ''
        if self.displacement:
            disp = ' + %i' % self.displacement

        return '%s [%s%s%s]' % (self.data_type, self.reg, scale, disp)


class NameOperand:
    def __init__(self, name: str, data_type: str,
                 displacement: int = None, struct_member: str = None):
        self.name = name
        self.data_type = data_type
        self.displacement = displacement
        self.struct_member = struct_member
        self.op_mask = None
        self.op_zero = None

    def to_str(self):
        if self.displacement:
            return '%s [%s + %i]' % (self.data_type, self.name, self.displacement)
        else:
            return '%s [%s]' % (self.data_type, self.name)


class ConstOperand:
    def __init__(self, value: int):
        self.value = value

    def to_str(self):
        return str(self.value)


class LabelOperand:
    def __init__(self, label: str, small_jump: bool = False, value: int = None):
        self.label = label
        self.small_jump = small_jump
        self.value = value

    def to_str(self):
        return self.label


class KeywordOperand:
    def __init__(self, name: str, iden: str):
        self.name = name
        self.iden = iden

    def to_str(self):
        return '%s %s' % (self.name, self.iden)


Operand = TypeVar('Operand', RegOperand, MemOperand, NameOperand, ConstOperand, LabelOperand, KeywordOperand)


class Instruction:
    def __init__(self, name: str, op1: Operand = None, op2: Operand = None,
                 op3: Operand = None, op4: Operand = None, repeat: str = None, lock: bool = False):
        self.name = name
        self.op1 = op1
        self.op2 = op2
        self.op3 = op3
        self.op4 = op4
        self.repeat = repeat
        self.lock = lock

    def to_str(self):
        ins = self.name
        if self.op1:
            ins += ' ' + self.op1.to_str()
        if self.op2:
            ins += ', ' + self.op2.to_str()
        if self.op3:
            ins += ', ' + self.op3.to_str()
        if self.op4:
            ins += ', ' + self.op4.to_str()
        return ins + '\n'


class EncodedInstruction:
    def __init__(self, name: str):
        self.name = name
        self.code = None
        self.disp_offset = None
        self.mem_name = None
        self.rel_label = None
        self.source = None


AsmInstruction = TypeVar('Asm', Directive, Label, Keyword, DataMembers, Instruction)