
VPANDD = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DB", "mod": "/r", "mbr": "1"}
}

VPANDQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DB", "mod": "/r", "mbr": "1"}
}

VPANDND = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "DF", "mod": "/r", "mbr": "1"}
}

VPANDNQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "DF", "mod": "/r", "mbr": "1"}
}

VPORD = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EB", "mod": "/r", "mbr": "1"}
}

VPORQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EB", "mod": "/r", "mbr": "1"}
}

VPXORD = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W0", "opcode": "EF", "mod": "/r", "mbr": "1"}
}

VPXORQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "EF", "mod": "/r", "mbr": "1"}
}

VPSRAQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "E2", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "E2", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "E2", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "E2", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "E2", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "E2", "mod": "/r"},

    "xmmxmmimm8": {"vvvv": "NDD", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib"},
    "ymmymmimm8": {"vvvv": "NDD", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib"},
    "zmmzmmimm8": {"vvvv": "NDD", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib"},

    "xmmmem128imm8": {"vvvv": "NDD", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib"},
    "ymmmem256imm8": {"vvvv": "NDD", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib"},
    "zmmmem512imm8": {"vvvv": "NDD", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib"},
    "xmmmem32imm8": {"vvvv": "NDD", "L": "128", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib", "mbr": "1"},
    "ymmmem32imm8": {"vvvv": "NDD", "L": "256", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib", "mbr": "1"},
    "zmmmem32imm8": {"vvvv": "NDD", "L": "512", "pp": "66", "mmmm": "0F", "W512": "W1", "opcode": "72", "mod": "/4", "imm": "ib", "mbr": "1"}
}

VMOVDQA32 = {
    "xmmxmm": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem128xmm": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},

    "ymmymm": {"L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem256ymm": {"L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},

    "zmmzmm": {"L": "512", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem512zmm": {"L": "512", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"}
}

VMOVDQA64 = {
    "xmmxmm": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem128xmm": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},

    "ymmymm": {"L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem256ymm": {"L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},

    "zmmzmm": {"L": "512", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem512zmm": {"L": "512", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"}
}

VMOVDQU32 = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem128xmm": {"L": "128", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},

    "ymmymm": {"L": "256", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem256ymm": {"L": "256", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},

    "zmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem512zmm": {"L": "512", "pp": "F3", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},
}

VMOVDQU64 = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem128xmm": {"L": "128", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},

    "ymmymm": {"L": "256", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem256ymm": {"L": "256", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},

    "zmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem512zmm": {"L": "512", "pp": "F3", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},
}

VMOVDQU8 = {
    "xmmxmm": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem128xmm": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},

    "ymmymm": {"L": "256", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem256ymm": {"L": "256", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},

    "zmmzmm": {"L": "512", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "6F", "mod": "/r"},
    "mem512zmm": {"L": "512", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "7F", "mod": "/r"},
}

VMOVDQU16 = {
    "xmmxmm": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem128xmm": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},

    "ymmymm": {"L": "256", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem256ymm": {"L": "256", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},

    "zmmzmm": {"L": "512", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "6F", "mod": "/r"},
    "mem512zmm": {"L": "512", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "7F", "mod": "/r"},
}

VPABSQ = {
    "xmmxmm": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r"},
    "xmmmem128": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r"},
    "ymmymm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r"},
    "ymmmem256": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r"},
    "zmmzmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r"},
    "zmmmem512": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r"},

    "xmmmem64": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r", "mbr": "1"},
    "ymmmem64": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r", "mbr": "1"},
    "zmmmem64": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "1F", "mod": "/r", "mbr": "1"}
}

VPMULLQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "40", "mod": "/r", "mbr": "1"}
}

VPMINUQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3B", "mod": "/r", "mbr": "1"}
}

VPMINSQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "39", "mod": "/r", "mbr": "1"}
}

VPMAXUQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3F", "mod": "/r", "mbr": "1"}
}

VPMAXSQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "WIG", "W512": "W1", "opcode": "3D", "mod": "/r", "mbr": "1"}
}

VBROADCASTF32X2 = {
    "ymmmem64": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "19", "mod": "/r"},
    "ymmxmm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "19", "mod": "/r"},
    "zmmmem64": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "19", "mod": "/r"},
    "zmmxmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "19", "mod": "/r"}
}

VBROADCASTF32X4 = {
    "ymmmem128": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "1A", "mod": "/r"},
    "zmmmem128": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "1A", "mod": "/r"}
}

VBROADCASTF64X2 = {
    "ymmmem128": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "1A", "mod": "/r"},
    "zmmmem128": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "1A", "mod": "/r"}
}

VBROADCASTF32X8 = {
    "zmmmem256": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "1B", "mod": "/r"}
}

VBROADCASTF64X4 = {
    "zmmmem256": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "1B", "mod": "/r"}
}

VEXTRACTF32X4 = {
    "xmmymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "19", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128ymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "19", "mod": "/r", "imm": "ib"},
    "xmmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "19", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "19", "mod": "/r", "imm": "ib"}
}

VEXTRACTF64X2 = {
    "xmmymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "19", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128ymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "19", "mod": "/r", "imm": "ib"},
    "xmmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "19", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "19", "mod": "/r", "imm": "ib"}
}

VEXTRACTF32X8 = {
    "ymmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "1B", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem256zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "1B", "mod": "/r", "imm": "ib"}
}

VEXTRACTF64X4 = {
    "ymmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "1B", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem256zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "1B", "mod": "/r", "imm": "ib"}
}

VINSERTF32X4 = {
    "ymmymmxmmimm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "18", "mod": "/r", "imm": "ib"},
    "ymmymmmem128imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "18", "mod": "/r", "imm": "ib"},
    "zmmzmmxmmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "18", "mod": "/r", "imm": "ib"},
    "zmmzmmmem128imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "18", "mod": "/r", "imm": "ib"}
}

VINSERTF64X2 = {
    "ymmymmxmmimm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "18", "mod": "/r", "imm": "ib"},
    "ymmymmmem128imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "18", "mod": "/r", "imm": "ib"},
    "zmmzmmxmmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "18", "mod": "/r", "imm": "ib"},
    "zmmzmmmem128imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "18", "mod": "/r", "imm": "ib"}
}

VINSERTF32X8 = {
    "zmmzmmymmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "1A", "mod": "/r", "imm": "ib"},
    "zmmzmmmem256imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "1A", "mod": "/r", "imm": "ib"}
}

VINSERTF64X4 = {
    "zmmzmmymmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "1A", "mod": "/r", "imm": "ib"},
    "zmmzmmmem256imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "1A", "mod": "/r", "imm": "ib"}
}

VBROADCASTI32X2 = {
    "xmmxmm": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "59", "mod": "/r"},
    "xmmmem64": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "59", "mod": "/r"},
    "ymmxmm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "59", "mod": "/r"},
    "ymmmem64": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "59", "mod": "/r"},
    "zmmxmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "59", "mod": "/r"},
    "zmmmem64": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "59", "mod": "/r"}
}

VBROADCASTI32X4 = {
    "ymmmem128": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "5A", "mod": "/r"},
    "zmmmem128": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "5A", "mod": "/r"}
}

VBROADCASTI64X2 = {
    "ymmmem128": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "5A", "mod": "/r"},
    "zmmmem128": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "5A", "mod": "/r"}
}

VBROADCASTI32X8 = {
    "zmmmem256": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "5B", "mod": "/r"}
}

VBROADCASTI64X4 = {
    "zmmmem256": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "5B", "mod": "/r"}
}

VEXTRACTI32X4 = {
    "xmmymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "39", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128ymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "39", "mod": "/r", "imm": "ib"},
    "xmmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "39", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "39", "mod": "/r", "imm": "ib"}
}

VEXTRACTI64X2 = {
    "xmmymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "39", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128ymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "39", "mod": "/r", "imm": "ib"},
    "xmmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "39", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem128zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "39", "mod": "/r", "imm": "ib"}
}

VEXTRACTI32X8 = {
    "ymmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "3B", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem256zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "3B", "mod": "/r", "imm": "ib"}
}

VEXTRACTI64X4 = {
    "ymmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "3B", "mod": "/r", "imm": "ib", 'rm': '1'},
    "mem256zmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "3B", "mod": "/r", "imm": "ib"}
}

VINSERTI32X4 = {
    "ymmymmxmmimm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "38", "mod": "/r", "imm": "ib"},
    "ymmymmmem128imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "38", "mod": "/r", "imm": "ib"},
    "zmmzmmxmmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "38", "mod": "/r", "imm": "ib"},
    "zmmzmmmem128imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "38", "mod": "/r", "imm": "ib"}
}

VINSERTI64X2 = {
    "ymmymmxmmimm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "38", "mod": "/r", "imm": "ib"},
    "ymmymmmem128imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "38", "mod": "/r", "imm": "ib"},
    "zmmzmmxmmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "38", "mod": "/r", "imm": "ib"},
    "zmmzmmmem128imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "38", "mod": "/r", "imm": "ib"}
}

VINSERTI32X8 = {
    "zmmzmmymmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "3A", "mod": "/r", "imm": "ib"},
    "zmmzmmmem256imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "3A", "mod": "/r", "imm": "ib"}
}

VINSERTI64X4 = {
    "zmmzmmymmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "3A", "mod": "/r", "imm": "ib"},
    "zmmzmmmem256imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "3A", "mod": "/r", "imm": "ib"}
}

VPERMW = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8D", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8D", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8D", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8D", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8D", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8D", "mod": "/r"}
}

VPSLLVW = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"}
}

VPSRAVQ = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "46", "mod": "/r", "mbr": "1"}
}

VPSRAVW = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "11", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "11", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "11", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "11", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "11", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "11", "mod": "/r"}
}

VPSRLVW = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "10", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "10", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "10", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "10", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "10", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "10", "mod": "/r"}
}

VRNDSCALESS = {
    "xmmxmmxmmimm8": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "0A", "mod": "/r", "imm": "ib"},
    "xmmxmmmem32imm8": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "0A", "mod": "/r", "imm": "ib"}
}

VRNDSCALEPS = {
    "xmmxmmimm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib"},
    "xmmmem128imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib"},
    "ymmymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib"},
    "ymmmem256imm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib"},
    "zmmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib"},
    "zmmmem512imm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib"},

    "xmmmem32imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib", "mbr": "1"},
    "ymmmem32imm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib", "mbr": "1"},
    "zmmmem32imm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "08", "mod": "/r", "imm": "ib", "mbr": "1"}
}

VRNDSCALESD = {
    "xmmxmmxmmimm8": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "0B", "mod": "/r", "imm": "ib"},
    "xmmxmmmem64imm8": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "0B", "mod": "/r", "imm": "ib"}
}

VRNDSCALEPD = {
    "xmmxmmimm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib"},
    "xmmmem128imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib"},
    "ymmymmimm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib"},
    "ymmmem256imm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib"},
    "zmmzmmimm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib"},
    "zmmmem512imm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib"},

    "xmmmem32imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib", "mbr": "1"},
    "ymmmem32imm8": {"L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib", "mbr": "1"},
    "zmmmem32imm8": {"L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "09", "mod": "/r", "imm": "ib", "mbr": "1"}
}

VALIGND = {
    "xmmxmmxmmimm8": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "imm": "ib"},
    "xmmxmmmem128imm8": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "imm": "ib"},
    "ymmymmymmimm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "imm": "ib"},
    "ymmymmmem256imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "imm": "ib"},
    "zmmzmmzmmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "imm": "ib"},
    "zmmzmmmem512imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "imm": "ib"},

    "xmmxmmmem32imm8": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "mbr": "1", "imm": "ib"},
    "ymmymmmem32imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "mbr": "1", "imm": "ib"},
    "zmmzmmmem32imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "03", "mod": "/r", "mbr": "1", "imm": "ib"}
}

VALIGNQ = {
    "xmmxmmxmmimm8": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "imm": "ib"},
    "xmmxmmmem128imm8": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "imm": "ib"},
    "ymmymmymmimm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "imm": "ib"},
    "ymmymmmem256imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "imm": "ib"},
    "zmmzmmzmmimm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "imm": "ib"},
    "zmmzmmmem512imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "imm": "ib"},

    "xmmxmmmem64imm8": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "mbr": "1", "imm": "ib"},
    "ymmymmmem64imm8": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "mbr": "1", "imm": "ib"},
    "zmmzmmmem64imm8": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "03", "mod": "/r", "mbr": "1", "imm": "ib"}
}

VBLENDMPS = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "65", "mod": "/r", "mbr": "1"}
}

VBLENDMPD = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "65", "mod": "/r", "mbr": "1"}
}


KADDW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "4A", "mod": "/r"},
}

KADDB = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "4A", "mod": "/r"},
}

KADDQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "4A", "mod": "/r"},
}

KADDD = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "4A", "mod": "/r"},
}

KANDW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "41", "mod": "/r"},
}

KANDB = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "41", "mod": "/r"},
}

KANDQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "41", "mod": "/r"},
}

KANDD = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "41", "mod": "/r"},
}

KANDNW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "42", "mod": "/r"},
}

KANDNB = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "42", "mod": "/r"},
}

KANDNQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "42", "mod": "/r"},
}

KANDND = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "42", "mod": "/r"},
}

KORW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "45", "mod": "/r"},
}

KORB = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "45", "mod": "/r"},
}

KORQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "45", "mod": "/r"},
}

KORD = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "45", "mod": "/r"},
}

KUNPCKBW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "4B", "mod": "/r"},
}

KUNPCKWD = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "4B", "mod": "/r"},
}

KUNPCKDQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "4B", "mod": "/r"},
}

KXNORW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "46", "mod": "/r"},
}

KXNORB = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "46", "mod": "/r"},
}

KXNORQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "46", "mod": "/r"},
}

KXNORD = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "46", "mod": "/r"},
}

KXORW = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W0", "opcode": "47", "mod": "/r"},
}

KXORB = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "47", "mod": "/r"},
}

KXORQ = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "mmmm": "0F", "W": "W1", "opcode": "47", "mod": "/r"},
}

KXORD = {
    "k64k64k64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "47", "mod": "/r"},
}

KMOVW = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "90", "mod": "/r"},
    "k64mem16": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "90", "mod": "/r"},
    "mem16k64": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "91", "mod": "/r"},
    "k64r32": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "92", "mod": "/r"},
    "r32k64": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "93", "mod": "/r"},
}

KMOVB = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "90", "mod": "/r"},
    "k64mem8": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "90", "mod": "/r"},
    "mem8k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "91", "mod": "/r"},
    "k64r32": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "92", "mod": "/r"},
    "r32k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "93", "mod": "/r"},
}

KMOVQ = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W1", "opcode": "90", "mod": "/r"},
    "k64mem64": {"L": "128", "mmmm": "0F", "W": "W1", "opcode": "90", "mod": "/r"},
    "mem64k64": {"L": "128", "mmmm": "0F", "W": "W1", "opcode": "91", "mod": "/r"},
    "k64r64": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "92", "mod": "/r"},
    "r64k64": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W1", "opcode": "93", "mod": "/r"},
}

KMOVD = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "90", "mod": "/r"},
    "k64mem32": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "90", "mod": "/r"},
    "mem32k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "91", "mod": "/r"},
    "k64r32": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "92", "mod": "/r"},
    "r32k64": {"L": "128", "pp": "F2", "mmmm": "0F", "W": "W0", "opcode": "93", "mod": "/r"},
}

KNOTW = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "44", "mod": "/r"},
}

KNOTB = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "44", "mod": "/r"},
}

KNOTQ = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W1", "opcode": "44", "mod": "/r"},
}

KNOTD = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "44", "mod": "/r"},
}

KORTESTW = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "98", "mod": "/r"},
}

KORTESTB = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "98", "mod": "/r"},
}

KORTESTQ = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W1", "opcode": "98", "mod": "/r"},
}

KORTESTD = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "98", "mod": "/r"},
}

KSHIFTLW = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "32", "mod": "/r", "imm": "ib"},
}

KSHIFTLB = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "32", "mod": "/r", "imm": "ib"},
}

KSHIFTLQ = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "33", "mod": "/r", "imm": "ib"},
}

KSHIFTLD = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "33", "mod": "/r", "imm": "ib"},
}

KSHIFTRW = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "30", "mod": "/r", "imm": "ib"},
}

KSHIFTRB = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "30", "mod": "/r", "imm": "ib"},
}

KSHIFTRQ = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W1", "opcode": "31", "mod": "/r", "imm": "ib"},
}

KSHIFTRD = {
    "k64k64imm8": {"L": "128", "pp": "66", "mmmm": "0F3A", "W": "W0", "opcode": "31", "mod": "/r", "imm": "ib"},
}

KTESTW = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W0", "opcode": "99", "mod": "/r"},
}

KTESTB = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W0", "opcode": "99", "mod": "/r"},
}

KTESTQ = {
    "k64k64": {"L": "128", "mmmm": "0F", "W": "W1", "opcode": "99", "mod": "/r"},
}

KTESTD = {
    "k64k64": {"L": "128", "pp": "66", "mmmm": "0F", "W": "W1", "opcode": "99", "mod": "/r"},
}

VPMOVQD = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "35", "mod": "/r", "rm": "1"},
    "mem64xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "35", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "35", "mod": "/r", "rm": "1"},
    "mem128ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "35", "mod": "/r"},
    "ymmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "35", "mod": "/r", "rm": "1"},
    "mem256zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "35", "mod": "/r"},
}

VPMOVSQD = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "25", "mod": "/r", "rm": "1"},
    "mem64xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "25", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "25", "mod": "/r", "rm": "1"},
    "mem128ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "25", "mod": "/r"},
    "ymmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "25", "mod": "/r", "rm": "1"},
    "mem256zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "25", "mod": "/r"},
}

VPMOVUSQD = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "15", "mod": "/r", "rm": "1"},
    "mem64xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "15", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "15", "mod": "/r", "rm": "1"},
    "mem128ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "15", "mod": "/r"},
    "ymmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "15", "mod": "/r", "rm": "1"},
    "mem256zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "15", "mod": "/r"},
}

VPMOVQB = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "32", "mod": "/r", "rm": "1"},
    "mem16xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "32", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "32", "mod": "/r", "rm": "1"},
    "mem32ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "32", "mod": "/r"},
    "xmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "32", "mod": "/r", "rm": "1"},
    "mem64zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "32", "mod": "/r"},
}

VPMOVSQB = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "22", "mod": "/r", "rm": "1"},
    "mem16xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "22", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "22", "mod": "/r", "rm": "1"},
    "mem32ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "22", "mod": "/r"},
    "xmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "22", "mod": "/r", "rm": "1"},
    "mem64zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "22", "mod": "/r"},
}

VPMOVUSQB = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r", "rm": "1"},
    "mem16xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r", "rm": "1"},
    "mem32ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
    "xmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r", "rm": "1"},
    "mem64zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "12", "mod": "/r"},
}

VPMOVQW = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "34", "mod": "/r", "rm": "1"},
    "mem32xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "34", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "34", "mod": "/r", "rm": "1"},
    "mem64ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "34", "mod": "/r"},
    "xmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "34", "mod": "/r", "rm": "1"},
    "mem128zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "34", "mod": "/r"},
}

VPMOVSQW = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "24", "mod": "/r", "rm": "1"},
    "mem32xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "24", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "24", "mod": "/r", "rm": "1"},
    "mem64ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "24", "mod": "/r"},
    "xmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "24", "mod": "/r", "rm": "1"},
    "mem128zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "24", "mod": "/r"},
}

VPMOVUSQW = {
    "xmmxmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "14", "mod": "/r", "rm": "1"},
    "mem32xmm": {"L": "128", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "14", "mod": "/r"},
    "xmmymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "14", "mod": "/r", "rm": "1"},
    "mem64ymm": {"L": "256", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "14", "mod": "/r"},
    "xmmzmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "14", "mod": "/r", "rm": "1"},
    "mem128zmm": {"L": "512", "pp": "F3", "mmmm": "0F38", "W": "W0", "opcode": "14", "mod": "/r"},
}

VCOMPRESSPS = {
    "xmmxmm": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "8A", "mod": "/r", "rm": "1"},
    "mem128xmm": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "8A", "mod": "/r"},
    "ymmymm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "8A", "mod": "/r", "rm": "1"},
    "mem256ymm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "8A", "mod": "/r"},
    "zmmzmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "8A", "mod": "/r", "rm": "1"},
    "mem512zmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "8A", "mod": "/r"},
}

VCOMPRESSPD = {
    "xmmxmm": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8A", "mod": "/r", "rm": "1"},
    "mem128xmm": {"L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8A", "mod": "/r"},
    "ymmymm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8A", "mod": "/r", "rm": "1"},
    "mem256ymm": {"L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8A", "mod": "/r"},
    "zmmzmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8A", "mod": "/r", "rm": "1"},
    "mem512zmm": {"L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "8A", "mod": "/r"},
}

VSCALEFSS = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2D", "mod": "/r"},
    "xmmxmmmem32": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2D", "mod": "/r"}
}

VSCALEFSD = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2D", "mod": "/r"},
    "xmmxmmmem64": {"vvvv": "NDS", "L": "LIG", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2D", "mod": "/r"}
}

VSCALEFPS = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r"},

    "xmmxmmmem32": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r", "mbr": "1"},
    "ymmymmmem32": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r", "mbr": "1"},
    "zmmzmmmem32": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W0", "opcode": "2C", "mod": "/r", "mbr": "1"}
}

VSCALEFPD = {
    "xmmxmmxmm": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r"},
    "xmmxmmmem128": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r"},
    "ymmymmymm": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r"},
    "ymmymmmem256": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r"},
    "zmmzmmzmm": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r"},
    "zmmzmmmem512": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r"},

    "xmmxmmmem64": {"vvvv": "NDS", "L": "128", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r", "mbr": "1"},
    "ymmymmmem64": {"vvvv": "NDS", "L": "256", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r", "mbr": "1"},
    "zmmzmmmem64": {"vvvv": "NDS", "L": "512", "pp": "66", "mmmm": "0F38", "W": "W1", "opcode": "2C", "mod": "/r", "mbr": "1"}
}