import ctypes
from ctypes import PYFUNCTYPE, c_void_p, c_size_t, c_ulong, WINFUNCTYPE, POINTER
from ctypes.wintypes import LPVOID, DWORD, LPDWORD, HANDLE, BOOL


LPTHREAD_START_ROUTINE = WINFUNCTYPE(DWORD, LPVOID)
SIZE_T = c_size_t

kernel32 = ctypes.windll.kernel32

# LPVOID WINAPI VirtualAlloc(LPVOID address, SIZE_T size, DWORD allocationType, DWORD protect)
VirtualAlloc = kernel32.VirtualAlloc
VirtualAlloc.restype = c_void_p
VirtualAlloc.argtypes = [c_void_p, c_size_t, c_ulong, c_ulong]


# BOOL WINAPI VirtualFree(LPVOID lpAddress, SIZE_T dwSize, DWORD  dwFreeType)
VirtualFree = kernel32.VirtualFree
VirtualFree.restype = ctypes.c_int
VirtualFree.argtypes = [c_void_p, c_size_t, c_ulong]


CreateThread = kernel32.CreateThread
CreateThread.argtypes = [LPVOID, SIZE_T, LPTHREAD_START_ROUTINE, LPVOID, DWORD, LPDWORD]
CreateThread.restype = HANDLE


WaitForSingleObject = kernel32.WaitForSingleObject
WaitForSingleObject.argtypes = [HANDLE, DWORD]
WaitForSingleObject.restype = DWORD

WaitForMultipleObjects = kernel32.WaitForMultipleObjects
WaitForMultipleObjects.argtypes = [DWORD, POINTER(HANDLE), BOOL, DWORD]
WaitForMultipleObjects.restype = DWORD


def allocate_win_exe_memory(size: int)-> int:
    PAGE_EXECUTE_READWRITE = 0x40
    MEM_COMMIT = 0x1000
    MEM_RESERVE = 0x2000
    address = VirtualAlloc(None, size, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE)
    return address


def release_win_exe_memory(address: int):
    MEM_RELEASE = 0x8000
    result = VirtualFree(address, 0, MEM_RELEASE)
    assert result != 0


def _run_thread(func_address: int):
    func = PYFUNCTYPE(None)
    func_pointer = func(func_address)
    func_pointer()
    return 0


start_procedure = LPTHREAD_START_ROUTINE(_run_thread)


def create_win_thread(func_address: int)-> int:
    return CreateThread(None, 0, start_procedure, func_address, 0, None)


def wait_win_thread(handle: int):
    WaitForSingleObject(handle, 0xFFFFFFFF)


def wait_win_threads(handles):
    cls_arr = HANDLE * len(handles)
    ctype_obj = cls_arr()
    ctype_obj[:] = handles
    WaitForMultipleObjects(len(handles), ctype_obj, 1, 0xFFFFFFFF)
