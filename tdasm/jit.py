from ctypes import (memmove, addressof, c_char_p, c_float, c_double,
                    c_int8, c_int16, c_int32, c_int64, c_uint8, c_uint16,
                    c_uint32, c_uint64)
from struct import pack, unpack
from typing import Sequence, Union

from .holders import ArrayMember, DataMember, AsmInstruction
from .tdasm import translate
from .memory import ExecutableMemory, MultiExecutableMemory
from .code import MachineCode
from .util import align


class _DataMember:
    def __init__(self, ctype_obj):
        self.ctype_obj = ctype_obj
        self.address = addressof(self.ctype_obj)

    def get(self):
        return self.ctype_obj.value

    def set(self, value):
        self.ctype_obj.value = value


class _ArrayDataMember:
    def __init__(self, ctype_obj):
        self.ctype_obj = ctype_obj
        self.address = addressof(self.ctype_obj)

    def get(self):
        return tuple(self.ctype_obj)

    def set(self, value):
        self.ctype_obj[:] = value


class _StringDataMember:
    def __init__(self, ctype_obj, address):
        self.ctype_obj = ctype_obj
        self.address = address
        self.set(ctype_obj.value.decode("ascii"))

    def get(self):
        memmove(self.ctype_obj, self.address, len(self.ctype_obj.value))
        return self.ctype_obj.value.decode("ascii")

    def set(self, value):
        if len(self.ctype_obj.value) != len(value):
            raise ValueError("Can only assign sequence of same size")
        self.ctype_obj.value = value.encode("ascii")
        memmove(self.address, self.ctype_obj, len(self.ctype_obj.value))


class DataMembers:
    def __init__(self, code: MachineCode, address: int):
        self._members = {}
        for name, member, offset in code.iter_members():
            addr = address + offset
            self._members[name] = self._create_data_member(member, addr)

    def _create_data_member(self, member, address):
        c_type_cls = {"int8": c_int8,
                      "int16": c_int16,
                      "int32": c_int32,
                      "uint8": c_uint8,
                      "uint16": c_uint16,
                      "uint32": c_uint32,
                      "int64": c_int64,
                      "uint64": c_uint64,
                      "float": c_float,
                      "double": c_double}

        if member.typ == "string":
            ctype_obj = c_char_p(member.value.encode("ascii"))
            d_member = _StringDataMember(ctype_obj, address)
        elif isinstance(member, ArrayMember):
            cls_arr = c_type_cls[member.typ] * len(member)
            ctype_obj = cls_arr.from_address(address)
            if member.values is not None:
                vals = [v for v in member.values]
                vals.extend([0] * (len(member) - len(member.values)))
                ctype_obj[:] = vals
            d_member = _ArrayDataMember(ctype_obj)
        elif isinstance(member, DataMember):
            ctype_obj = c_type_cls[member.typ].from_address(address)
            if member.value is not None:
                ctype_obj.value = member.value
            d_member = _DataMember(ctype_obj)
        else:
            raise ValueError("Unsupported data member type! ", member)
        return d_member

    def __getitem__(self, name: str):
        return self._members[name].get()

    def __setitem__(self, name: str, value):
        self._members[name].set(value)

    def addressof(self, name: str):
        return self._members[name].address


class BaseJit:
    def __init__(self, source: Union[str, Sequence[AsmInstruction]], jit_funcs=[], standalone: bool = True):
        self._source = source
        self._code = translate(source, standalone=standalone)
        self._jit_funcs = jit_funcs

    def _reallocate(self, code: MachineCode,
                    code_address: int,
                    data_members: DataMembers,
                    labels={}):

        rip = code_address
        raw_code = b''
        for inst in code.iter_instructions():
            rip += len(inst.code)
            if inst.mem_name is not None:
                addr = data_members.addressof(inst.mem_name)
                doff = inst.disp_offset
                disp = unpack('i', inst.code[doff:doff + 4])[0]
                addr = addr + disp - rip  # rip relative addressing
                code = inst.code[:doff] + pack("i", addr) + inst.code[doff + 4:]
            elif inst.rel_label is not None:
                lbl, size = inst.rel_label
                diff = labels[lbl] - rip
                if size == 1:
                    code = inst.code[:len(inst.code) - 1] + pack('b', diff)
                else:
                    code = inst.code[:len(inst.code) - 4] + pack('i', diff)
            else:
                code = inst.code

            raw_code += code
        return raw_code

    def code_size(self):
        size = align(self._code.code_section_size(), 64)
        for func in self._jit_funcs:
            size += func.code_size()
        return size

    def data_size(self):
        size = align(self._code.data_section_size(), 64)
        for func in self._jit_funcs:
            size += func.data_size()
        return size

    def _load_jit_funcs(self, address, data_address, labels):

        new_labels = {}
        raw_code = b''
        for func in self._jit_funcs:
            address, data_address, code = func._load_jit_funcs(address, data_address, new_labels)
            raw_code += code

            labels[func._name] = address
            members = DataMembers(func._code, data_address)
            code = func._reallocate(func._code, address, members, new_labels)
            address = address + len(code)
            diff = align(address, 64) - address
            raw_code += code + b'\x00' * diff
            address += diff
            data_address += align(func._code.data_section_size(), 64)

        return address, data_address, raw_code

    def _prepare_raw_code(self, code_address, data_address):
        func_address = code_address + align(self._code.code_section_size(), 64)
        func_data = data_address + align(self._code.data_section_size(), 64)
        labels = {}
        _, _, funcs_code = self._load_jit_funcs(func_address, func_data, labels)

        data_members = DataMembers(self._code, data_address)
        raw_code = self._reallocate(self._code, code_address,
                                    data_members,
                                    labels)
        diff = align(self._code.code_section_size(), 64) - len(raw_code)
        raw_code = raw_code + b'\x00' * diff + funcs_code
        return raw_code, data_members


class FuncJit(BaseJit):
    def __init__(self, source: Union[str, Sequence[AsmInstruction]], name: str, jit_funcs=[]):
        super().__init__(source, jit_funcs, standalone=False)
        self._name = name


class TdasmJit(BaseJit):
    def __init__(self, source: Union[str, Sequence[AsmInstruction]], jit_funcs=[]):
        super().__init__(source, jit_funcs)
        self._prepare()

    def _prepare(self):
        self._exe = ExecutableMemory(self.code_size(),
                                     data_size=self.data_size())
        code, data_members = self._prepare_raw_code(self._exe.code_address(), self._exe.data_address())
        self._data_members = data_members
        self._exe.load_code(code)

    def set_param(self, name, value):
        self._data_members[name] = value

    def get_param(self, name):
        return self._data_members[name]

    def run(self):
        self._exe.run()


class MultiTdasmJit(BaseJit):
    def __init__(self, source: Union[str, Sequence[AsmInstruction]], nthreads: int = 4, jit_funcs=[]):
        super().__init__(source, jit_funcs)
        self._nthreads = nthreads
        self._prepare()

    def _prepare(self):
        self._exe = MultiExecutableMemory(self.code_size(),
                                          data_size=self.data_size(),
                                          nthreads=self._nthreads)

        self._data_members = []
        codes = []
        for caddr, daddr in zip(self._exe.code_address(), self._exe.data_address()):
            code, data_members = self._prepare_raw_code(caddr, daddr)
            codes.append(code)
            self._data_members.append(data_members)

        self._exe.load_code(codes)

    def set_param(self, name, value, thread_idx=None):
        if thread_idx:
            self._data_members[thread_idx][name] = value
        else:
            for members in self._data_members:
                members[name] = value

    def get_param(self, name):
        return [members[name] for members in self._data_members]

    def run(self):
        self._exe.run()
