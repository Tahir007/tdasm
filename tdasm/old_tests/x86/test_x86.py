
import unittest
import random
import x86

#Note: SetInt and GetInt methods doesn't check for overflow

class X86Test(unittest.TestCase):

    def setUp(self):
        random.seed()
        self.i64 = random.randint(-9000000000000000, 9000000000000000)
        self.i32 = random.randint(-2000000000, 2000000000)
        self.i16 = random.randint(-32000, 32000)
        self.i8 = random.randint(-128, 127)
        self.ui64 = random.randint(0, 18000000000000000)
        self.ui32 = random.randint(0, 4000000000)
        self.ui16 = random.randint(0, 65000)
        self.ui8 = random.randint(0, 255)
        self.double = random.random()
        self.mem = x86.MemData(1024)
    
    def test_i64(self):
        x86.SetInt64(self.mem.ptr(), self.i64, 0)
        k = x86.GetInt64(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.i64)

    def test_i32(self):
        x86.SetInt32(self.mem.ptr(), self.i32, 0)
        k = x86.GetInt32(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.i32)

    def test_i16(self):
        x86.SetInt16(self.mem.ptr(), self.i16, 0)
        k = x86.GetInt16(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.i16)

    def test_i8(self):
        x86.SetInt8(self.mem.ptr(), self.i8, 0)
        k = x86.GetInt8(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.i8)
    
    def test_ui64(self):
        x86.SetUInt64(self.mem.ptr(), self.ui64, 0)
        k = x86.GetUInt64(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.ui64)

    def test_ui32(self):
        x86.SetUInt32(self.mem.ptr(), self.ui32, 0)
        k = x86.GetUInt32(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.ui32)

    def test_ui16(self):
        x86.SetUInt16(self.mem.ptr(), self.ui16, 0)
        k = x86.GetUInt16(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.ui16)

    def test_ui8(self):
        x86.SetUInt8(self.mem.ptr(), self.ui8, 0)
        k = x86.GetUInt8(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.ui8)

    def test_double(self):
        x86.SetDouble(self.mem.ptr(), self.double, 0)
        k = x86.GetDouble(self.mem.ptr(), 0, 0)
        self.assertEqual(k, self.double)

    def test_float(self):
        x86.SetFloat(self.mem.ptr(), self.double, 0)
        k = x86.GetFloat(self.mem.ptr(), 0, 0)
        self.assertAlmostEqual(k, self.double)

    def test_exe(self):
        m = x86.MemExe(1)
        x86.SetInt32(m.ptr(), self.i32, 0)
        k = x86.GetInt32(m.ptr(), 0, 0)
        self.assertEqual(k, self.i32)

    def test_get_set(self):
        d = b"abc"
        x86.SetData(self.mem.ptr(), d)
        k = x86.GetData(self.mem.ptr(), 3)
        self.assertEqual(k, d)

    def test_execute(self):
        m = x86.MemExe(1)
        e = b"\xc3"
        x86.SetData(m.ptr(), e)
        x86.ExecuteModule(m.ptr())


if __name__ == "__main__":
    unittest.main()


