
import unittest
from tdasm import translate, Runtime


class SSE2DataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse2_data_transfer1(self):
        code = """
            #DATA
            double p1[2]
            double val1[2] = 2.3, 2e-6

            #CODE
            movapd xmm3, oword [val1]
            movapd xmm1, xmm3
            movupd xmm4, xmm1
            movupd oword [p1], xmm4
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][0], 2.3)
        self.assertAlmostEqual(ds['p1'][1], 2e-6)

if __name__ == "__main__":
    unittest.main()
