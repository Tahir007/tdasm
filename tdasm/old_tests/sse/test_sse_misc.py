
import math
import unittest
from tdasm import translate, Runtime


class SSEMisc(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse_misc_1(self):
        code = """
            #DATA
            int32 p1
            int32 val[4] = 2, 5, 1, 1
            int16 pp

            #CODE
            movups xmm4, oword [val]
            mov edx, 25
            vpinsrw xmm4, xmm4, edx, 4
            vpextrw ecx, xmm4, 4
            mov word [pp], cx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['pp'], 25)

if __name__ == "__main__":
    unittest.main()
