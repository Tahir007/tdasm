
import math
import unittest
from tdasm import translate, Runtime


class SSEDataArithmetic(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse_arithmetic1(self):
        code = """
            #DATA
            float p1[4]
            float val1[4] = 2.3, 2e-6, 10, -0.0

            #CODE
            movaps xmm3, oword [val1]
            sqrtps xmm4, xmm3
            movups oword [p1], xmm4
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][0], math.sqrt(2.3))
        self.assertAlmostEqual(ds['p1'][1], math.sqrt(2e-6))
        self.assertAlmostEqual(ds['p1'][2], math.sqrt(10))
        self.assertEqual(str(ds['p1'][3]), '-0.0')

if __name__ == "__main__":
    unittest.main()
