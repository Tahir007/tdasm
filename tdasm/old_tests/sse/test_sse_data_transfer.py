
import unittest
from tdasm import translate, Runtime


class SSEDataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse_data_transfer1(self):
        code = """
            #DATA
            float p1[4]
            float val1[4] = 2.3, 2e-6, 10, -2.5

            #CODE
            movaps xmm3, oword [val1]
            movaps xmm1, xmm3
            movups oword [p1], xmm1
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][0], 2.3)
        self.assertAlmostEqual(ds['p1'][1], 2e-6)
        self.assertAlmostEqual(ds['p1'][2], 10)
        self.assertAlmostEqual(ds['p1'][3], -2.5)

    def test_sse_data_transfer2(self):
        code = """
            #DATA
            float p1[4]
            float val1[4] = 2.3, 2e-6, 10, -2.5

            #CODE
            movhps xmm4, qword [val1]
            movups oword [p1], xmm4
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][2], 2.3)
        self.assertAlmostEqual(ds['p1'][3], 2e-6)

if __name__ == "__main__":
    unittest.main()
