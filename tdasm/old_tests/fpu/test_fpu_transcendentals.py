
import unittest
import math
from tdasm import translate, Runtime


class FPUTranscendentalTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_fpu_transcendental1(self):
        code = """
            #DATA
            double p1, p2
            double val1 = 2.4444
            double val2 = 1.4444
            #CODE
            finit
            fld qword [val1]
            fld qword [val2]
            fsin
            fstp qword [p1]
            fcos
            fstp qword [p2]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], math.sin(1.4444))
        self.assertAlmostEqual(ds['p2'], math.cos(2.4444))

    def test_fpu_transcendental2(self):
        code = """
            #DATA
            double p1, p2
            double val1 = 2.4444
            #CODE
            finit
            fld qword [val1]
            fsincos
            fstp qword [p1]
            fstp qword [p2]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], math.cos(2.4444))
        self.assertAlmostEqual(ds['p2'], math.sin(2.4444))

    def test_fpu_transcendental3(self):
        code = """
            #DATA
            double p1
            double val1 = 0.4
            #CODE
            finit
            fld qword [val1]
            f2xm1
            fstp qword [p1]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 2 ** 0.4 - 1.0)


if __name__ == "__main__":
    unittest.main()
