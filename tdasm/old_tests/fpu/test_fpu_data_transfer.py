
import unittest
from tdasm import translate, Runtime


class FPUDataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_fpu_data_tranfer1(self):
        code = """
            #DATA
            float p1
            double p2
            float val1 = 2.23
            double val2 = 5.5555
            #CODE
            finit
            fld dword [val1]
            fst dword [p1]

            fld qword [val2]
            fstp qword [p2]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 2.23)
        self.assertAlmostEqual(ds['p2'], 5.5555)

    def test_fpu_data_tranfer2(self):
        code = """
            #DATA
            int32 p1
            int64 p2
            int32 val1 = 44
            int64 val2 = 99
            #CODE
            finit
            fild dword [val1]
            fist dword [p1]

            fild qword [val2]
            fistp qword [p2]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 44)
        self.assertAlmostEqual(ds['p2'], 99)

    def test_fpu_data_tranfer3(self):
        code = """
            #DATA
            double p1
            double val1 = 3.334
            double val2 = 2.222
            #CODE
            finit
            fld qword [val1]
            fld qword [val2]
            fxch st1
            fst qword [p1]

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 3.334)

    def test_fpu_data_tranfer4(self):
        code = """
            #DATA
            double p1
            double val1 = 3.334
            double val2 = 2.222
            #CODE
            finit
            fld qword [val1]
            fld qword [val2]
            mov eax, 0
            cmp eax, 0
            fcmove st0, st1
            fst qword [p1]

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 3.334)

if __name__ == "__main__":
    unittest.main()
