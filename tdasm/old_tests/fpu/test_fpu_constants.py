
import unittest
import math
from tdasm import translate, Runtime


class FPUConstantsTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_fpu_constants1(self):
        code = """
            #DATA
            double p1, p3, p4, p5, p6, p7
            double p2 = 2.2
            #CODE
            finit
            fldpi
            fst qword [p1]
            fldz
            fst qword [p2]
            fld1
            fst qword [p3]
            fldlg2
            fst qword [p4]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], math.pi)
        self.assertAlmostEqual(ds['p2'], 0.0)
        self.assertAlmostEqual(ds['p3'], 1.0)
        self.assertAlmostEqual(ds['p4'], math.log10(2))


if __name__ == "__main__":
    unittest.main()
