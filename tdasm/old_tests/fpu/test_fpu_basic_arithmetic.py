
import unittest
import math
from tdasm import translate, Runtime


class FPUBasicArithmeticTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_fpu_basic_arithmetic1(self):
        code = """
            #DATA
            double val = 3.4
            int32 val2 = -2
            double p1
            #CODE
            finit
            fldpi
            fld1
            fadd qword [val]
            fiadd dword [val2]
            fadd  st0, st1
            fst qword [p1]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 1.0 + 3.4 - 2 + math.pi)

    def test_fpu_basic_arithmetic2(self):
        code = """
            #DATA
            double val = 3.4
            float val2 = -2.0
            double p1
            #CODE
            finit
            fldpi
            fld1
            fmul qword [val]
            fmul dword [val2]
            fmul st0, st1
            fst qword [p1]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 1.0 * 3.4 * -2 * math.pi)

    def test_fpu_basic_arithmetic3(self):
        code = """
            #DATA
            double val = 22.3
            double val2 = 3.3
            double p1
            #CODE
            finit
            fld qword [val2]
            fld qword [val]
            fprem
            fst qword [p1]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], 22.3 % 3.3)

    def test_fpu_basic_arithmetic4(self):
        code = """
            #DATA
            double p1
            #CODE
            finit
            fldpi
            fsqrt
            fst qword [p1]
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'], math.sqrt(math.pi))

if __name__ == "__main__":
    unittest.main()
