
import unittest
from tdasm import translate, Runtime


class SSE41MISC(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse41_misc_1(self):
        code = """
            #DATA
            double val1[2] = 2.2, 3.3
            double val2[2] = 1.1, 5.5
            ;uint64 mask[2] = 0, 0
            uint64 mask[2] = 0xFFFFFFFFFFFFFFFF, 0
            double val3[2]

            #CODE
            movups xmm13, oword [mask]
            movups xmm1, oword [val1]
            movups xmm2, oword [val2]
            ;blendvpd xmm1, xmm2
            vblendvpd xmm1, xmm1, xmm2, xmm13
            movups oword [val3], xmm1
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        val = ds['val3']
        self.assertEqual(val[0], 1.1)
        self.assertEqual(val[1], 3.3)

if __name__ == "__main__":
    unittest.main()
