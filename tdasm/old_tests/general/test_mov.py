
import unittest
import random
from tdasm import translate, Runtime


class MovTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_mov1(self):
        code = """
            #DATA
            uint8 a, b
            uint16 d, e
            uint32 i, j
            #CODE
            mov al, 165
            mov bx, 4567
            mov edx, 123456
            mov byte [a], al
            mov word [d], bx
            mov dword [i], edx
            mov byte [b], 44
            mov word [e], 1345
            mov dword [j], 222334455
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['a'], 165)
        self.assertEqual(ds['b'], 44)
        self.assertEqual(ds['d'], 4567)
        self.assertEqual(ds['e'], 1345)
        self.assertEqual(ds['i'], 123456)
        self.assertEqual(ds['j'], 222334455)

    def test_mov2(self):
        code = """
            #DATA
            uint8 a1, a2, b1, b2, c1, c2
            #CODE
            mov al, byte[a1]
            mov byte[a2], al
            mov dl, byte[b1]
            mov byte[b2], dl

            mov al, byte [c1]
            mov bl, al
            mov cl, bl
            mov dl, cl
            mov cl, dl
            mov bl, cl
            mov byte [c2], bl
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["a1"] = random.randint(0, 250)
        ds["b1"] = random.randint(0, 250)
        ds["c1"] = random.randint(0, 250)
        run.run("test")
        self.assertEqual(ds['a1'], ds['a2'])
        self.assertEqual(ds['b1'], ds['b2'])
        self.assertEqual(ds['c1'], ds['c2'])

    def test_mov3(self):
        code = """
            #DATA
            uint16 a1, a2, b1, b2
            #CODE
            mov ax, word[a1]
            mov word[a2], ax

            mov ax, word [b1]
            mov cx, ax
            mov dx, cx
            mov bx, dx
            mov word [b2], bx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["a1"] = random.randint(0, 65536)
        ds["b1"] = random.randint(0, 65536)
        run.run("test")
        self.assertEqual(ds['a1'], ds['a2'])
        self.assertEqual(ds['b1'], ds['b2'])

    def test_mov4(self):
        code = """
            #DATA
            uint32 a1, a2, b1, b2
            #CODE
            mov eax, dword[a1]
            mov dword[a2], eax

            mov esp, dword [b1]
            mov ebp, esp
            mov ecx, ebp
            mov ebx, ecx
            mov dword [b2], ebx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["a1"] = random.randint(0, 2000000000)
        ds["b1"] = random.randint(0, 2000000000)
        run.run("test")
        self.assertEqual(ds['a1'], ds['a2'])
        self.assertEqual(ds['b1'], ds['b2'])

    def test_mov5(self):
        code = """
            #DATA
            uint32 arr[16]
            #CODE
            mov eax, dword[arr + 4]
            mov dword [arr + 12], eax

            mov edx, dword [arr + 12 + 8]
            mov dword [arr + 16 + 20], edx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["arr"] = tuple([random.randint(0, 2000000000) for x in range(16)])
        run.run("test")
        arr = ds["arr"]
        self.assertEqual(arr[1], arr[3])
        self.assertEqual(arr[5], arr[9])

    def test_mov6(self):
        code = """
            #DATA
            uint8 a, b, c, h
            uint16 d, e, f
            uint32 i, j, k
            uint64 m, n, p
            #CODE
            mov al, 165
            mov sil, 13
            mov r12l, 146
            mov byte [a], al
            mov byte [b], 44
            mov byte [c], sil
            mov byte [h], r12l

            mov bx, 4567
            mov r14w, 456
            mov word [d], bx
            mov word [f], r14w
            mov word [e], 1345

            mov edx, 123456
            mov r12d, 123678
            mov dword [i], edx
            mov dword [k], r12d
            mov dword [j], 222334455

            mov rdx, 34343434343434
            mov r11, 22002200220022
            mov qword [m], rdx
            mov qword [n], r11
            mov qword[p], 1000000000
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['a'], 165)
        self.assertEqual(ds['c'], 13)
        self.assertEqual(ds['h'], 146)
        self.assertEqual(ds['b'], 44)

        self.assertEqual(ds['d'], 4567)
        self.assertEqual(ds['e'], 1345)
        self.assertEqual(ds['f'], 456)

        self.assertEqual(ds['i'], 123456)
        self.assertEqual(ds['k'], 123678)
        self.assertEqual(ds['j'], 222334455)

        self.assertEqual(ds['m'], 34343434343434)
        self.assertEqual(ds['n'], 22002200220022)
        self.assertEqual(ds['p'], 1000000000)

    def test_mov7(self):
        code = """
            #DATA
            uint8 a1, a2, b1, b2, c1, c2, d1, d2
            #CODE
            mov al, byte[a1]
            mov byte[a2], al
            mov dl, byte[b1]
            mov byte[b2], dl

            mov al, byte [c1]
            mov bl, al
            mov cl, bl
            mov dl, cl
            mov cl, dl
            mov bl, cl
            mov byte [c2], bl
            mov r10l, byte[d1]
            mov r12l, r10l
            mov r13l, r12l
            mov r14l, r13l
            mov byte [d2], r14l
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["a1"] = random.randint(0, 250)
        ds["b1"] = random.randint(0, 250)
        ds["c1"] = random.randint(0, 250)
        ds["d1"] = random.randint(0, 250)
        run.run("test")
        self.assertEqual(ds['a1'], ds['a2'])
        self.assertEqual(ds['b1'], ds['b2'])
        self.assertEqual(ds['c1'], ds['c2'])
        self.assertEqual(ds['d1'], ds['d2'])

    def test_mov8(self):
        code = """
            #DATA
            uint16 a1, a2, b1, b2, c1, c2
            #CODE
            mov ax, word[a1]
            mov word[a2], ax

            mov ax, word [b1]
            mov cx, ax
            mov dx, cx
            mov bx, dx
            mov word [b2], bx
            mov r10w, word [c1]
            mov r12w, r10w
            mov r15w, r12w
            mov word [c2], r15w
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["a1"] = random.randint(0, 65536)
        ds["b1"] = random.randint(0, 65536)
        ds["c1"] = random.randint(0, 65536)
        run.run("test")
        self.assertEqual(ds['a1'], ds['a2'])
        self.assertEqual(ds['b1'], ds['b2'])
        self.assertEqual(ds['c1'], ds['c2'])

    def test_mov9(self):
        code = """
            #DATA
            uint32 a1, a2, b1, b2, c1, c2
            uint64 m1
            #CODE
            mov eax, dword[a1]
            mov dword[a2], eax

            mov esp, dword [b1]
            mov ebp, esp
            mov ecx, ebp
            mov ebx, ecx
            mov dword [b2], ebx
            mov r9d, dword [c1]
            mov r15d, r9d
            mov dword [c2], r15d
            mov rcx, 789
            mov qword[m1], rcx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["a1"] = random.randint(0, 2000000000)
        ds["b1"] = random.randint(0, 2000000000)
        ds["c1"] = random.randint(0, 2000000000)
        run.run("test")
        self.assertEqual(ds['a1'], ds['a2'])
        self.assertEqual(ds['b1'], ds['b2'])
        self.assertEqual(ds['c1'], ds['c2'])
        self.assertEqual(ds['m1'], 789)


if __name__ == "__main__":
    unittest.main()
