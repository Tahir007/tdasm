
import unittest
from tdasm import translate, Runtime


class LogicalInstTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_logical1(self):
        code = """
            #DATA
            uint32 p1, p2, p3
            uint8 p4
            uint32 val = 5

            #CODE
            mov eax, 5
            mov edx, 6
            and eax, edx
            mov dword [p1], eax

            mov esi, 6
            xor esi, 4
            mov dword [p2], esi

            mov edx, 9
            or edx, dword [val]
            mov dword [p3], edx

            mov al, 15
            not al
            mov byte [p4], al


        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 4)
        self.assertEqual(ds['p2'], 2)
        self.assertEqual(ds['p3'], 13)
        self.assertEqual(ds['p4'], 240)

if __name__ == "__main__":
    unittest.main()
