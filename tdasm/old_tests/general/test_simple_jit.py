
from tdasm import TdasmJit, MultiTdasmJit


def test_simple_jit():
    asm = """
#DATA
int32 x
#CODE
mov dword [x], 32
"""
    jit = TdasmJit(asm)
    jit.run()
    result = jit.get_param('x')
    assert result == 32


def test_simple_multi_jit():
    asm = """
#DATA
int32 x
int32 y
#CODE
mov eax, dword[x]
mov dword [y], eax
"""

    mjit = MultiTdasmJit(asm, nthreads=4)
    mjit.set_param('x', 4)  # set 4 to all threads
    mjit.set_param('x', 6, thread_idx=2)
    mjit.run()
    result = mjit.get_param('y')
    assert result[0] == 4
    assert result[1] == 4
    assert result[2] == 6
    assert result[3] == 4

