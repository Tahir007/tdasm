
import unittest
from tdasm import translate, Runtime


class ControlTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_control_transfer1(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            mov eax, 44
            mov dword [p1], eax
            jmp end
            mov eax, 55
            mov dword [p1], eax
            end:
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 44)

    def test_control_transfer2(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            mov eax, 44
            mov ebx, 44
            mov dword [p1], eax
            cmp eax, ebx
            je the_end
            mov eax, 55
            mov dword [p1], eax
            the_end:
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 44)

    def test_control_transfer3(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            mov eax, 5
            mov ecx, 5
            acum:
            add eax, 5
            loop acum
            mov dword [p1], eax
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 30)

    def test_control_transfer4(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            jmp after_routine

            routine:
            mov dword [p1], 23
            ret

            after_routine:
            mov dword [p1], 11
            call routine
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 23)

if __name__ == "__main__":
    unittest.main()
