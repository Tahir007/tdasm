
import unittest
from tdasm import translate, Runtime


class BinaryArithmeticInsts(unittest.TestCase):
    def setUp(self):
        pass

    def test_bin_arithmetic1(self):
        code = """
            #DATA
            uint32 p1, p2

            #CODE
            mov eax, 5
            mov edx, 6
            add eax, edx
            mov dword [p1], eax

            mov cl, 145
            mov dl, 200
            add cl, dl

            mov edi, 5
            adc edi, 7
            mov dword [p2], edi
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 11)
        self.assertEqual(ds['p2'], 13)

    def test_bin_arithmetic2(self):
        code = """
            #DATA
            uint32 p1, p2

            #CODE
            mov eax, 55
            mov edx, 125
            sub edx, eax
            mov dword [p1], edx

            mov cl, 145
            mov dl, 200
            add cl, dl

            mov edi, 20
            sbb edi, 7
            mov dword [p2], edi
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 70)
        self.assertEqual(ds['p2'], 12)

    def test_bin_arithmetic3(self):
        code = """
            #DATA
            int32 p1, p2
            uint32 val = 8

            #CODE
            mov esi, 6
            imul edx, esi, 4
            mov dword [p1], edx

            mov eax, 7
            mul dword [val]
            mov dword [p2], eax

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 24)
        self.assertEqual(ds['p2'], 56)

    def test_bin_arithmetic4(self):
        code = """
            #DATA
            int32 p1, p2
            int32 val = -10

            #CODE
            mov eax, 100
            mov edx, 0
            idiv dword [val]
            mov dword [p1], eax

            mov eax, 50
            mov edx, 0
            mov esi, 5
            div esi
            mov dword [p2], eax

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], -10)
        self.assertEqual(ds['p2'], 10)

    def test_bin_arithmetic5(self):
        code = """
            #DATA
            uint32 p1
            uint64 p2
            int32 p3

            #CODE
            mov edx, 100
            inc edx
            mov dword [p1], edx

            mov rbx, 147
            dec rbx
            mov qword [p2], rbx

            mov esi, 55
            neg esi
            mov dword [p3], esi
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 101)
        self.assertEqual(ds['p2'], 146)
        self.assertEqual(ds['p3'], -55)

if __name__ == "__main__":
    unittest.main()
