
import unittest
from tdasm import translate, Runtime


class GeneralMiscInstructionsTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_misc_inst1(self):
        code = """
            #DATA
            uint64 p1
            uint32 val = 4

            #CODE
            lea rax, dword [val]
            mov qword [p1], rax
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], ds.address_off('val'))

    def test_misc_inst2(self):
        code = """
            #DATA
            uint8 p1[4]
            uint8 vals[4] = 22, 44, 77, 99

            #CODE
            movbe ebx, dword [vals]
            mov dword [p1], ebx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        vals = ds['p1']
        self.assertEqual(vals[0], 99)
        self.assertEqual(vals[1], 77)
        self.assertEqual(vals[2], 44)
        self.assertEqual(vals[3], 22)

if __name__ == "__main__":
    unittest.main()
