
import unittest
from tdasm import translate, Runtime


class ShiftRotateInstTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_shift_rotate1(self):
        code = """
            #DATA
            uint32 p1, p2, p3
            uint64 p4

            #CODE
            mov eax, 32
            sar eax, 1
            mov dword [p1], eax

            mov esi, 64
            shr esi, 1
            mov dword [p2], esi

            mov edx, 3
            sal edx, 2
            mov dword [p3], edx

            mov r11, 5
            shl r11, 2
            mov qword [p4], r11
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 16)
        self.assertEqual(ds['p2'], 32)
        self.assertEqual(ds['p3'], 12)
        self.assertEqual(ds['p4'], 20)

    def test_shift_rotate2(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            mov eax, 16
            mov ebx, -1
            shld eax, ebx, 2
            mov dword [p1], eax
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 67)

    def test_shift_rotate3(self):
        code = """
            #DATA
            uint32 p1, p2

            #CODE
            mov eax, 16
            rol eax, 1
            mov dword [p1], eax

            mov esi, 64
            ror esi, 1
            mov dword [p2], esi
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 32)
        self.assertEqual(ds['p2'], 32)

if __name__ == "__main__":
    unittest.main()
