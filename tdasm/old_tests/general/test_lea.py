
import unittest
import random
from tdasm import translate, Runtime


class LeaTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_lea1(self):
        code = """
            #DATA
            uint32 arr[16]
            #CODE
            lea rax, byte [arr]

            mov ebx, dword[rax]
            mov dword [rax + 4], ebx

            mov edx, dword [rax + 12 + 8]
            mov dword [rax + 16 + 20], edx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["arr"] = tuple([random.randint(0, 2000000000) for x in range(16)])
        run.run("test")
        arr = ds["arr"]
        self.assertEqual(arr[0], arr[1])
        self.assertEqual(arr[5], arr[9])

    def test_lea2(self):
        code = """
            #DATA
            uint32 arr[16]
            #CODE
            lea rax, byte [arr]
            mov ebx, 4
            mov ecx, dword[rax + rbx]
            mov dword [rax + 2*rbx], ecx

            mov edx, dword [rax + rbx*4 + 4]
            mov dword [rax + rbx*8 + 4 + 8], edx

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["arr"] = tuple([random.randint(0, 2000000000) for x in range(16)])
        run.run("test")
        arr = ds["arr"]
        self.assertEqual(arr[1], arr[2])
        self.assertEqual(arr[5], arr[11])

    def test_lea3(self):
        code = """
            #DATA
            uint64 arr[16]
            #CODE
            lea r11, byte [arr]
            mov rbx, qword[r11]
            mov qword [r11 + 8], rbx

            mov r14, 3
            mov qword [r11 + 8*r14], rbx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        ds["arr"] = tuple([random.randint(0, 2000000000) for x in range(16)])
        run.run("test")
        arr = ds["arr"]
        self.assertEqual(arr[0], arr[1])
        self.assertEqual(arr[0], arr[3])


if __name__ == "__main__":
    unittest.main()
