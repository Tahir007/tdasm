
import unittest
from tdasm import translate, Runtime


class DataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_data_transfer1(self):
        code = """
            #DATA
            uint32 p1, p2, p3, p4
            uint32 val = 34

            #CODE
            mov eax, 22
            mov ebx, 77
            xchg eax, ebx
            mov dword [p1], eax
            mov dword [p2], ebx
            mov edx, 55
            mov edi, 89
            xchg edx, edi
            mov dword [p3], edx
            mov ecx, 11
            xchg ecx, dword [val]
            mov dword [p4], ecx


        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 77)
        self.assertEqual(ds['p2'], 22)
        self.assertEqual(ds['p3'], 89)
        self.assertEqual(ds['p4'], 34)

    def test_data_transfer2(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            mov eax, 0x11223344
            bswap eax
            mov dword[p1], eax

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], int('0x44332211', 16))

    def test_data_transfer3(self):
        code = """
            #DATA
            uint32 p1, p2, p3
            uint32 val = 55

            #CODE
            mov eax, 22
            mov ebx, 44
            xadd eax, ebx
            mov dword[p1], eax
            mov dword[p2], ebx

            mov edi, 35
            xadd dword [val], edi
            mov dword[p3], edi

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 66)
        self.assertEqual(ds['p2'], 22)
        self.assertEqual(ds['p3'], 55)
        self.assertEqual(ds['val'], 90)

    def test_data_transfer4(self):
        code = """
            #DATA
            uint32 p1

            #CODE
            mov eax, 22
            mov edx, 22
            mov ebx, 44
            cmpxchg edx, ebx
            mov dword[p1], edx

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 44)

    def test_data_transfer5(self):
        code = """
            #DATA
            uint64 p1, p2, p3, p4, p5
            uint64 val = 456

            #CODE
            mov rbx, 11223344556677
            push rbx
            pop rcx
            mov qword[p1], rcx

            push 23
            push 5000000
            push qword [val]

            pop qword [p2]
            pop r14
            pop rsi

            mov qword[p3], r14
            mov qword[p4], rsi
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 11223344556677)
        self.assertEqual(ds['p2'], 456)
        self.assertEqual(ds['p3'], 5000000)
        self.assertEqual(ds['p4'], 23)

    def test_data_transfer6(self):
        code = """
            #DATA
            int16 p1
            int32 p2
            int16 p3, p4

            #CODE
            mov al, -3
            cbw
            mov word [p1], ax
            mov ax, -8
            cwde
            mov dword [p2], eax
            mov ax, -25
            mov dx, 2
            cwd
            mov word [p3], ax
            mov word [p4], dx
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], -3)
        self.assertEqual(ds['p2'], -8)
        self.assertEqual(ds['p4'], -1)

    def test_data_transfer7(self):
        code = """
            #DATA
            int32 p1
            int64 p2, p3, p4

            #CODE
            mov bx, -69
            movsx ecx, bx
            mov dword [p1], ecx
            mov edx, -4
            movsx rsi, edx
            mov qword [p2], rsi
            mov ax, -7
            movsx r15, ax
            mov qword [p3], r15

            mov bl, -1
            movzx rcx, bl
            mov qword [p4], rcx


        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], -69)
        self.assertEqual(ds['p2'], -4)
        self.assertEqual(ds['p3'], -7)
        self.assertEqual(ds['p4'], 255)


if __name__ == "__main__":
    unittest.main()
