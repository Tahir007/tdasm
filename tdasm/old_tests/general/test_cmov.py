
import unittest
from tdasm import translate, Runtime


class CMovTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_cmov1(self):
        code = """
            #DATA
            uint32 p1, p2, p3
            #CODE
            mov edx, 22
            mov ebx, 11
            mov eax, 10
            cmp eax, 10
            cmove ebx, edx
            mov dword [p1], ebx
            cmovns esi, ebx
            mov dword [p2], esi
            mov esi, 3
            cmovs esi, edx
            mov dword [p3], esi
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 22)
        self.assertEqual(ds['p2'], 22)
        self.assertEqual(ds['p3'], 3)

    def test_cmov2(self):
        code = """
            #DATA
            int32 p1, p2, p3
            #CODE
            mov eax, 4
            mov ebx, 11
            cmp eax, ebx

            mov edx, 5
            mov ecx, 3
            mov esi, 2
            cmovge esi, edx
            mov dword [p1], esi
            cmovb esi, edx
            mov dword [p2], esi


        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 2)
        self.assertEqual(ds['p2'], 5)


if __name__ == "__main__":
    unittest.main()
