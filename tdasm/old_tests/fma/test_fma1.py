
import unittest
from tdasm import Runtime, translate


class FmaTest(unittest.TestCase):

    def check_value(self, t, value, places=7):
        for v in t:
            self.assertAlmostEqual(v, value, places)

    def get_ds(self, code):
        self.runtime = Runtime()
        mc = translate(code)
        ds = self.runtime.load("test", mc)
        self.runtime.run("test")
        return ds

    def asm_code_ps(self, inst):
        code = """
            #DATA
            float v1[8] = 2.2, 2.2, 2.2, 2.2, 2.2, 2.2, 2.2, 2.2
            float v2[8] = 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2
            float v3[8] = 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6
            float v4[8] = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
            #CODE
            vmovaps ymm0, yword[v1]
            vmovaps ymm1, yword[v2]
            vmovaps ymm2, yword[v3]
        """
        code += inst + """
            vmovaps yword [v4], ymm0
        """
        return code

    def asm_code_pd(self, inst):
        code = """
            #DATA
            double v1[4] = 2.2, 2.2, 2.2, 2.2
            double v2[4] = 1.2, 1.2, 1.2, 1.2
            double v3[4] = 1.6, 1.6, 1.6, 1.6
            double v4[4] = 0.0, 0.0, 0.0, 0.0
            #CODE
            vmovapd ymm0, yword[v1]
            vmovapd ymm1, yword[v2]
            vmovapd ymm2, yword[v3]
        """
        code += inst + """
            vmovapd yword [v4], ymm0
        """
        return code

    def ps_test(self, code, value, n=4):
        ds = self.get_ds(code)
        v1 = ds['v4']
        if n == 1:
            self.check_value(v1[0:1], value, places=6)
        elif n == 4:
            self.check_value(v1[0:4], value, places=6)
        elif n == 8:
            self.check_value(v1, value, places=6)
        else:
            raise ValueError("Wrong n = ", n)

    def pd_test(self, code, value, n=2):
        ds = self.get_ds(code)
        v1 = ds['v4']
        if n == 1:
            self.check_value(v1[0:1], value)
        elif n == 2:
            self.check_value(v1[0:2], value)
        elif n == 4:
            self.check_value(v1, value)
        else:
            raise ValueError("Wrong n = ", n)

    def ps_test_add_sub(self, code, value1, value2, n=2):
        ds = self.get_ds(code)
        v1 = ds['v4']
        if n == 2:
            tup = (v1[0], v1[2])
            self.check_value(tup, value1, places=6)
            tup = (v1[1], v1[3])
            self.check_value(tup, value2, places=6)
        elif n == 4:
            tup = (v1[0], v1[2], v1[4], v1[6])
            self.check_value(tup, value1, places=6)
            tup = (v1[1], v1[3], v1[5], v1[7])
            self.check_value(tup, value2, places=6)
        else:
            raise ValueError("Wrong n = ", n)

    def pd_test_add_sub(self, code, value1, value2, n=1):
        ds = self.get_ds(code)
        v1 = ds['v4']
        if n == 1:
            self.check_value(v1[0:1], value1)
            self.check_value(v1[1:2], value2)
        elif n == 2:
            self.check_value(v1[0:1], value1)
            self.check_value(v1[1:2], value2)
            self.check_value(v1[2:3], value1)
            self.check_value(v1[3:4], value2)
        else:
            raise ValueError("Wrong n = ", n)

    def test_vfmadd132ps(self):
        val = 2.2 * 1.6 + 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfmadd132ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfmadd132ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmadd132ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

        code = self.asm_code_ps('vfmadd132ps xmm0, xmm1, oword [v3]')
        self.ps_test(code, val, n=4)

    def test_vfmadd132pd(self):
        val = 2.2 * 1.6 + 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfmadd132sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfmadd132pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfmadd132pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfmadd213ps(self):
        val = 1.2 * 2.2 + 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfmadd213ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfmadd213ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmadd213ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

        code = self.asm_code_ps('vfmadd213ps xmm0, xmm1, oword [v3]')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmadd213ps ymm0, ymm1, yword [v3]')
        self.ps_test(code, val, n=8)

    def test_vfmadd213pd(self):
        val = 1.2 * 2.2 + 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfmadd213sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfmadd213pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfmadd213pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfmadd231ps(self):
        val = 1.2 * 1.6 + 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfmadd231ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfmadd231ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmadd231ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfmadd231pd(self):
        val = 1.2 * 1.6 + 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfmadd231sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfmadd231pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfmadd231pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfmaddsub132ps(self):
        val1 = 2.2 * 1.6 - 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 2.2 * 1.6 + 1.2
        code = self.asm_code_ps('vfmaddsub132ps xmm0, xmm1, xmm2')
        self.ps_test_add_sub(code, val1, val2, n=2)
        code = self.asm_code_ps('vfmaddsub132ps ymm0, ymm1, ymm2')
        self.ps_test_add_sub(code, val1, val2, n=4)

    def test_vfmaddsub132pd(self):
        val1 = 2.2 * 1.6 - 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 2.2 * 1.6 + 1.2
        code = self.asm_code_pd('vfmaddsub132pd xmm0, xmm1, xmm2')
        self.pd_test_add_sub(code, val1, val2, n=1)
        code = self.asm_code_pd('vfmaddsub132pd ymm0, ymm1, ymm2')
        self.pd_test_add_sub(code, val1, val2, n=2)

    def test_vfmaddsub213ps(self):
        val1 = 1.2 * 2.2 - 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 2.2 + 1.6
        code = self.asm_code_ps('vfmaddsub213ps xmm0, xmm1, xmm2')
        self.ps_test_add_sub(code, val1, val2, n=2)
        code = self.asm_code_ps('vfmaddsub213ps ymm0, ymm1, ymm2')
        self.ps_test_add_sub(code, val1, val2, n=4)

    def test_vfmaddsub213pd(self):
        val1 = 1.2 * 2.2 - 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 2.2 + 1.6
        code = self.asm_code_pd('vfmaddsub213pd xmm0, xmm1, xmm2')
        self.pd_test_add_sub(code, val1, val2, n=1)
        code = self.asm_code_pd('vfmaddsub213pd ymm0, ymm1, ymm2')
        self.pd_test_add_sub(code, val1, val2, n=2)

    def test_vfmaddsub231ps(self):
        val1 = 1.2 * 1.6 - 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 1.6 + 2.2
        code = self.asm_code_ps('vfmaddsub231ps xmm0, xmm1, xmm2')
        self.ps_test_add_sub(code, val1, val2, n=2)
        code = self.asm_code_ps('vfmaddsub231ps ymm0, ymm1, ymm2')
        self.ps_test_add_sub(code, val1, val2, n=4)

    def test_vfmaddsub231pd(self):
        val1 = 1.2 * 1.6 - 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 1.6 + 2.2
        code = self.asm_code_pd('vfmaddsub231pd xmm0, xmm1, xmm2')
        self.pd_test_add_sub(code, val1, val2, n=1)
        code = self.asm_code_pd('vfmaddsub231pd ymm0, ymm1, ymm2')
        self.pd_test_add_sub(code, val1, val2, n=2)

    def test_vfmsubadd132ps(self):
        val1 = 2.2 * 1.6 + 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 2.2 * 1.6 - 1.2
        code = self.asm_code_ps('vfmsubadd132ps xmm0, xmm1, xmm2')
        self.ps_test_add_sub(code, val1, val2, n=2)
        code = self.asm_code_ps('vfmsubadd132ps ymm0, ymm1, ymm2')
        self.ps_test_add_sub(code, val1, val2, n=4)

    def test_vfmsubadd132pd(self):
        val1 = 2.2 * 1.6 + 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 2.2 * 1.6 - 1.2
        code = self.asm_code_pd('vfmsubadd132pd xmm0, xmm1, xmm2')
        self.pd_test_add_sub(code, val1, val2, n=1)
        code = self.asm_code_pd('vfmsubadd132pd ymm0, ymm1, ymm2')
        self.pd_test_add_sub(code, val1, val2, n=2)

    def test_vfmsubadd213ps(self):
        val1 = 1.2 * 2.2 + 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 2.2 - 1.6
        code = self.asm_code_ps('vfmsubadd213ps xmm0, xmm1, xmm2')
        self.ps_test_add_sub(code, val1, val2, n=2)
        code = self.asm_code_ps('vfmsubadd213ps ymm0, ymm1, ymm2')
        self.ps_test_add_sub(code, val1, val2, n=4)

    def test_vfmsubadd213pd(self):
        val1 = 1.2 * 2.2 + 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 2.2 - 1.6
        code = self.asm_code_pd('vfmsubadd213pd xmm0, xmm1, xmm2')
        self.pd_test_add_sub(code, val1, val2, n=1)
        code = self.asm_code_pd('vfmsubadd213pd ymm0, ymm1, ymm2')
        self.pd_test_add_sub(code, val1, val2, n=2)

    def test_vfmsubadd231ps(self):
        val1 = 1.2 * 1.6 + 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 1.6 - 2.2
        code = self.asm_code_ps('vfmsubadd231ps xmm0, xmm1, xmm2')
        self.ps_test_add_sub(code, val1, val2, n=2)
        code = self.asm_code_ps('vfmsubadd231ps ymm0, ymm1, ymm2')
        self.ps_test_add_sub(code, val1, val2, n=4)

    def test_vfmsubadd231pd(self):
        val1 = 1.2 * 1.6 + 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        val2 = 1.2 * 1.6 - 2.2
        code = self.asm_code_pd('vfmsubadd231pd xmm0, xmm1, xmm2')
        self.pd_test_add_sub(code, val1, val2, n=1)
        code = self.asm_code_pd('vfmsubadd231pd ymm0, ymm1, ymm2')
        self.pd_test_add_sub(code, val1, val2, n=2)

    def test_vfmsub132ps(self):
        val = 2.2 * 1.6 - 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfmsub132ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfmsub132ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmsub132ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfmsub213ps(self):
        val = 1.2 * 2.2 - 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfmsub213ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfmsub213ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmsub213ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfmsub231ps(self):
        val = 1.2 * 1.6 - 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfmsub231ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfmsub231ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfmsub231ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfmsub132pd(self):
        val = 2.2 * 1.6 - 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfmsub132sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfmsub132pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfmsub132pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfmsub213pd(self):
        val = 1.2 * 2.2 - 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfmsub213sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfmsub213pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfmsub213pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfmsub231pd(self):
        val = 1.2 * 1.6 - 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfmsub231sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfmsub231pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfmsub231pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfnmadd132ps(self):
        val = -(2.2 * 1.6) + 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfnmadd132ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfnmadd132ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmadd132ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

        code = self.asm_code_ps('vfnmadd132ps xmm0, xmm1, oword [v3]')
        self.ps_test(code, val, n=4)

    def test_vfnmadd213ps(self):
        val = -(1.2 * 2.2) + 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfnmadd213ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfnmadd213ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmadd213ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

        code = self.asm_code_ps('vfnmadd213ps xmm0, xmm1, oword [v3]')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmadd213ps ymm0, ymm1, yword [v3]')
        self.ps_test(code, val, n=8)

    def test_vfnmadd231ps(self):
        val = -(1.2 * 1.6) + 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfnmadd231ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfnmadd231ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmadd231ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfnmadd132pd(self):
        val = -(2.2 * 1.6) + 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfnmadd132sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfnmadd132pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfnmadd132pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfnmadd213pd(self):
        val = -(1.2 * 2.2) + 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfnmadd213sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfnmadd213pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfnmadd213pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfnmadd231pd(self):
        val = -(1.2 * 1.6) + 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfnmadd231sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfnmadd231pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfnmadd231pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfnmsub132ps(self):
        val = -(2.2 * 1.6) - 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfnmsub132ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfnmsub132ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmsub132ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfnmsub213ps(self):
        val = -(1.2 * 2.2) - 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfnmsub213ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfnmsub213ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmsub213ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfnmsub231ps(self):
        val = -(1.2 * 1.6) - 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_ps('vfnmsub231ss xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=1)
        code = self.asm_code_ps('vfnmsub231ps xmm0, xmm1, xmm2')
        self.ps_test(code, val, n=4)
        code = self.asm_code_ps('vfnmsub231ps ymm0, ymm1, ymm2')
        self.ps_test(code, val, n=8)

    def test_vfnmsub132pd(self):
        val = -(2.2 * 1.6) - 1.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfnmsub132sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfnmsub132pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfnmsub132pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfnmsub213pd(self):
        val = -(1.2 * 2.2) - 1.6  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfnmsub213sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfnmsub213pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfnmsub213pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)

    def test_vfnmsub231pd(self):
        val = -(1.2 * 1.6) - 2.2  # ymm0 = 2.2, ymm1 = 1.2, ymm2 = 1.6
        code = self.asm_code_pd('vfnmsub231sd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=1)
        code = self.asm_code_pd('vfnmsub231pd xmm0, xmm1, xmm2')
        self.pd_test(code, val, n=2)
        code = self.asm_code_pd('vfnmsub231pd ymm0, ymm1, ymm2')
        self.pd_test(code, val, n=4)


if __name__ == "__main__":
    unittest.main()
