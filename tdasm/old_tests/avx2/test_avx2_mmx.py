
import unittest
from tdasm import translate, Runtime


class AVX2MMXTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_avx2_mmx1(self):
        code = """
            #DATA
            int16 val1[16] = 69, -25, 360, -600, 22, 5555, 33, 22, 3, 4, 445, 6, 7, 5, 9, -8
            int16 val2[16] = 45, -99, 450, 11, 1, -666, -25, 33, 33, 555, -888, 1, 3, 56, 66, 33
            int8 p1[32]

            #CODE
            vmovaps ymm2, yword [val1]
            vpacksswb ymm3, ymm2, yword [val2]
            vmovaps yword [p1], ymm3
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 69)
        self.assertEqual(ds['p1'][1], -25)
        self.assertEqual(ds['p1'][2], 127)
        self.assertEqual(ds['p1'][3], -128)
        self.assertEqual(ds['p1'][4], 22)
        self.assertEqual(ds['p1'][5], 127)
        self.assertEqual(ds['p1'][6], 33)
        self.assertEqual(ds['p1'][7], 22)

        self.assertEqual(ds['p1'][8], 45)
        self.assertEqual(ds['p1'][9], -99)
        self.assertEqual(ds['p1'][10], 127)
        self.assertEqual(ds['p1'][11], 11)
        self.assertEqual(ds['p1'][12], 1)
        self.assertEqual(ds['p1'][13], -128)
        self.assertEqual(ds['p1'][14], -25)
        self.assertEqual(ds['p1'][15], 33)

        self.assertEqual(ds['p1'][25], 127)
        self.assertEqual(ds['p1'][26], -128)
        self.assertEqual(ds['p1'][30], 66)
        self.assertEqual(ds['p1'][31], 33)

if __name__ == "__main__":
    unittest.main()
