
import unittest
from tdasm import translate, Runtime


class AVXSSE2DataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse2_data_transfer1(self):
        code = """
            #DATA
            double p1[4]
            double val1[4] = 2.3, 2e-6, 10, -2.5

            #CODE
            vmovapd ymm3, yword [val1]
            vmovapd ymm1, ymm3
            vmovapd ymm6, ymm1
            vmovupd yword [p1], ymm6
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][0], 2.3)
        self.assertAlmostEqual(ds['p1'][1], 2e-6)
        self.assertAlmostEqual(ds['p1'][2], 10)
        self.assertAlmostEqual(ds['p1'][3], -2.5)

if __name__ == "__main__":
    unittest.main()
