
import unittest
from tdasm import translate, Runtime


class AVXSSEDataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_sse_avx_data_transfer1(self):
        code = """
            #DATA
            float p1[4]
            float val1[4] = 2.3, 2e-6, 10, -2.5

            #CODE
            vmovaps xmm3, oword [val1]
            vmovaps xmm1, xmm3
            vmovups oword [p1], xmm1
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][0], 2.3)
        self.assertAlmostEqual(ds['p1'][1], 2e-6)
        self.assertAlmostEqual(ds['p1'][2], 10)
        self.assertAlmostEqual(ds['p1'][3], -2.5)

    def test_sse_avx_data_transfer2(self):
        code = """
            #DATA
            float p1[4]
            float val1[4] = 2.3, 2e-6, 10, -2.5
            float val2[4] = 1.2, 1.1, 7.2, -7.5

            #CODE
            vmovaps xmm3, oword [val1]
            vmovhps xmm4, xmm3, qword [val2]
            vmovups oword [p1], xmm4
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertAlmostEqual(ds['p1'][0], 2.3)
        self.assertAlmostEqual(ds['p1'][1], 2e-6)
        self.assertAlmostEqual(ds['p1'][2], 1.2)
        self.assertAlmostEqual(ds['p1'][3], 1.1)

if __name__ == "__main__":
    unittest.main()
