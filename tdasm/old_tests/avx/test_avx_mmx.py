
import unittest
from tdasm import translate, Runtime


class AVXMMXTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_avx_mmx1(self):
        code = """
            #DATA
            uint32 p1
            uint64 p2, p3
            uint64 val = 1122445566

            #CODE
            mov eax, 55
            vmovd xmm2, eax
            vmovd esi, xmm2
            mov dword[p1], esi

            mov rcx, 47
            vmovq xmm0, rcx
            vmovq rdx, xmm0
            mov qword[p2], rdx

            vmovq xmm2, qword [val]
            vmovq xmm9, xmm2
            vmovq qword [p3], xmm9

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 55)
        self.assertEqual(ds['p2'], 47)
        self.assertEqual(ds['p3'], 1122445566)

    def test_avx_mmx2(self):
        code = """
            #DATA
            int16 val1[8] = 69, -25, 360, -600, 22, 5555, 33, 22
            int16 val2[8] = 45, -99, 450, 11, 1, -666, -25, 33
            int8 p1[16]

            #CODE
            vmovaps xmm2, oword [val1]
            vpacksswb xmm3, xmm2, oword [val2]
            vmovaps oword [p1], xmm3

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 69)
        self.assertEqual(ds['p1'][1], -25)
        self.assertEqual(ds['p1'][2], 127)
        self.assertEqual(ds['p1'][3], -128)
        self.assertEqual(ds['p1'][4], 22)
        self.assertEqual(ds['p1'][5], 127)
        self.assertEqual(ds['p1'][6], 33)
        self.assertEqual(ds['p1'][7], 22)

        self.assertEqual(ds['p1'][8], 45)
        self.assertEqual(ds['p1'][9], -99)
        self.assertEqual(ds['p1'][10], 127)
        self.assertEqual(ds['p1'][11], 11)
        self.assertEqual(ds['p1'][12], 1)
        self.assertEqual(ds['p1'][13], -128)
        self.assertEqual(ds['p1'][14], -25)
        self.assertEqual(ds['p1'][15], 33)

    def test_avx_mmx3(self):
        code = """
            #DATA
            int8 val1[16] = 69, -25, 50, 24, 33, 11, 22, 88
            int8 val2[16] = 45, -99, 36, 11, 77, 66, 55, 44
            int8 p1[16]

            #CODE
            vmovaps xmm2, oword [val1]
            vpunpcklbw xmm2, xmm2, oword [val2]
            vmovaps oword [p1], xmm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 69)
        self.assertEqual(ds['p1'][1], 45)
        self.assertEqual(ds['p1'][2], -25)
        self.assertEqual(ds['p1'][3], -99)

    def test_avx_mmx4(self):
        code = """
            #DATA
            int32 val1[8] = 69, -25, 50, 24, 33, 11, 22, 88
            int32 val2[8] = 45, -99, 36, 11, 77, 66, 55, 44
            int32 p1[8]

            #CODE
            vmovaps ymm2, yword [val1]
            vpsubd ymm2, ymm2, yword [val2]
            vmovaps yword [p1], ymm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 24)
        self.assertEqual(ds['p1'][1], 74)
        self.assertEqual(ds['p1'][6], -33)
        self.assertEqual(ds['p1'][7], 44)

    def test_avx_mmx5(self):
        code = """
            #DATA
            int32 val1[8] = 69, -25, 50, 24, 33, 11, 22, 88
            int32 val2[8] = 45, -99, 36, 11, 77, 66, 55, 44
            int32 p1[8]

            #CODE
            vmovaps ymm2, yword [val1]
            vpslld ymm2, ymm2, 1
            vmovaps yword [p1], ymm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        p1 = ds['p1']
        self.assertEqual(p1[0], 69 * 2)
        self.assertEqual(p1[1], -25 * 2)
        self.assertEqual(p1[7], 88 * 2)

if __name__ == "__main__":
    unittest.main()
