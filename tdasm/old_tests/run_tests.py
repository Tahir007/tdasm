
import unittest
from unittest import TestLoader
suite = TestLoader().discover('x86')
suite.addTests(TestLoader().discover('general'))
suite.addTests(TestLoader().discover('fpu'))
suite.addTests(TestLoader().discover('mmx'))
suite.addTests(TestLoader().discover('sse'))
suite.addTests(TestLoader().discover('sse2'))
suite.addTests(TestLoader().discover('sse41'))
suite.addTests(TestLoader().discover('avx'))
suite.addTests(TestLoader().discover('avx2'))
suite.addTests(TestLoader().discover('fma'))
unittest.TextTestRunner(verbosity=5).run(suite)
