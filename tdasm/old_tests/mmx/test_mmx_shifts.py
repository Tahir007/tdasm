
import unittest
from tdasm import translate, Runtime


class MMXShiftTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_mmx_shifts1(self):
        code = """
            #DATA
            int32 val1[8] = 69, -25, 50, 24, 33, 11, 22, 88
            int32 val2[8] = 45, -99, 36, 11, 77, 66, 55, 44
            int32 p1[8]

            #CODE
            movaps xmm2, oword [val1]
            pslld xmm2, 1
            movaps oword [p1], xmm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        p1 = ds['p1']
        self.assertEqual(p1[0], 69 * 2)
        self.assertEqual(p1[1], -25 * 2)

    def test_mmx_shifts2(self):
        code = """
            #DATA
            int32 val1[8] = 69, -25, 50, 24, 33, 11, 22, 88
            int32 val2[8] = 1, 0, 0, 0, 0, 0, 0, 0
            int32 p1[8]

            #CODE
            movaps xmm2, oword [val1]
            movaps xmm3, oword [val2]
            pslld xmm2, xmm3
            movaps oword [p1], xmm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        p1 = ds['p1']
        self.assertEqual(p1[0], 69 * 2)
        self.assertEqual(p1[1], -25 * 2)

if __name__ == "__main__":
    unittest.main()
