
import unittest
from tdasm import translate, Runtime


class MMXConversionTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_mmx_conversions1(self):
        code = """
            #DATA
            int16 val1[4] = 69, -25, 360, -600
            int16 val2[4] = 45, -99, 450, 11
            int8 p1[8]

            #CODE
            movq mm0, qword [val1]
            packsswb mm0, qword [val2]
            movq qword [p1], mm0

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 69)
        self.assertEqual(ds['p1'][1], -25)
        self.assertEqual(ds['p1'][2], 127)
        self.assertEqual(ds['p1'][3], -128)
        self.assertEqual(ds['p1'][4], 45)
        self.assertEqual(ds['p1'][5], -99)
        self.assertEqual(ds['p1'][6], 127)
        self.assertEqual(ds['p1'][7], 11)

    def test_mmx_conversions2(self):
        code = """
            #DATA
            int16 val1[4] = 69, -25, 360, -600
            int16 val2[4] = 45, -99, 450, 11
            int16 p1[4]

            #CODE
            movq mm0, qword [val1]
            punpckhwd mm0, qword [val2]
            movq qword [p1], mm0
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 360)
        self.assertEqual(ds['p1'][1], 450)
        self.assertEqual(ds['p1'][2], -600)
        self.assertEqual(ds['p1'][3], 11)

    def test_mmx_conversions3(self):
        code = """
            #DATA
            int8 val1[8] = 69, -25, 50, 24, 33, 11, 22, 88
            int8 val2[8] = 45, -99, 36, 11, 77, 66, 55, 44
            int8 p1[8]

            #CODE
            movq mm0, qword [val1]
            punpcklbw mm0, dword [val2]
            movq qword [p1], mm0
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 69)
        self.assertEqual(ds['p1'][1], 45)
        self.assertEqual(ds['p1'][2], -25)
        self.assertEqual(ds['p1'][3], -99)

    def test_mmx_conversions4(self):
        code = """
            #DATA
            int8 val1[16] = 69, -25, 50, 24, 33, 11, 22, 88
            int8 val2[16] = 45, -99, 36, 11, 77, 66, 55, 44
            int8 p1[16]

            #CODE
            movaps xmm2, oword [val1]
            punpcklbw xmm2, oword [val2]
            movaps oword [p1], xmm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'][0], 69)
        self.assertEqual(ds['p1'][1], 45)
        self.assertEqual(ds['p1'][2], -25)
        self.assertEqual(ds['p1'][3], -99)

if __name__ == "__main__":
    unittest.main()
