
import unittest
from tdasm import translate, Runtime


class MMXArithmeticTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_mmx_arithmetic1(self):
        code = """
            #DATA
            int32 val1[4] = 33, -25, 11, 50
            int32 val2[4] = 45, -99, 450, 11
            int32 p1[4]
            int32 p2[4]

            #CODE
            movq mm0, qword [val1]
            paddd mm0, qword [val2]
            movq qword [p1], mm0

            movaps xmm2, oword [val1]
            paddd xmm2, oword [val2]
            movaps oword [p2], xmm2
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        p1 = ds['p1']
        p2 = ds['p2']
        self.assertEqual(p1[0], 78)
        self.assertEqual(p1[1], -124)
        self.assertEqual(p2[0], 78)
        self.assertEqual(p2[1], -124)
        self.assertEqual(p2[2], 461)
        self.assertEqual(p2[3], 61)

    def test_mmx_arithmetic2(self):
        code = """
            #DATA
            int32 val1[4] = 250, 150, 100, 50
            int32 val2[4] = 50, 100, 150, 200
            int32 p1[4]
            int32 p2[4]

            #CODE
            movq mm0, qword [val1]
            psubd mm0, qword [val2]
            movq qword [p1], mm0
            movaps xmm4, oword [val1]
            psubd xmm4, oword [val2]
            movaps oword [p2], xmm4
        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        p1 = ds['p1']
        p2 = ds['p2']
        self.assertEqual(p1[0], 200)
        self.assertEqual(p1[1], 50)
        self.assertEqual(p2[2], -50)
        self.assertEqual(p2[3], -150)

if __name__ == "__main__":
    unittest.main()
