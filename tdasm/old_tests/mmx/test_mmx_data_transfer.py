
import unittest
from tdasm import translate, Runtime


class MMXDataTransferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_mmx_data_tranfer1(self):
        code = """
            #DATA
            uint32 p1, p2
            uint64 p3, p4

            #CODE
            mov eax, 55
            movd mm2, eax
            movd esi, mm2
            mov dword[p1], esi

            mov edx, 22
            movd xmm5, edx
            movd edi, xmm5
            mov dword[p2], edi

            mov r15, 11223344556677
            movq mm2, r15
            movq r14, mm2
            mov qword[p3], r14

            mov r9, 9988776655443322
            movq xmm5, r9
            movq rcx, xmm5
            mov qword[p4], rcx

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 55)
        self.assertEqual(ds['p2'], 22)
        self.assertEqual(ds['p3'], 11223344556677)
        self.assertEqual(ds['p4'], 9988776655443322)

    def test_mmx_data_tranfer2(self):
        code = """
            #DATA
            uint64 p1, p2
            uint64 val = 11223344
            uint64 val2 = 44556677

            #CODE

            movq mm2, qword [val]
            movq mm4, mm2
            movq qword [p1], mm4

            movq xmm3, qword [val2]
            movq xmm1, xmm3
            movq qword [p2], xmm1

        """
        mc = translate(code)
        run = Runtime()
        ds = run.load("test", mc)
        run.run("test")
        self.assertEqual(ds['p1'], 11223344)
        self.assertEqual(ds['p2'], 44556677)

if __name__ == "__main__":
    unittest.main()
