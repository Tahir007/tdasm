
import platform
from ctypes import c_char_p, memmove, PYFUNCTYPE
from .util import align


if platform.system() == "Windows":
    from ._win_memory import (allocate_win_exe_memory, release_win_exe_memory,
                              create_win_thread, wait_win_threads)
    alocate_exe_memory = allocate_win_exe_memory
    release_exe_memory = release_win_exe_memory
    create_thread = create_win_thread
    wait_threads = wait_win_threads
else:
    raise NotImplementedError()


class ExecutableMemory:
    def __init__(self, size: int, data_size: int =0):
        self._size = size
        self._data_size = data_size
        size = align(self._size + 64, 65536) + align(data_size, 65536)
        self._address = alocate_exe_memory(size)

    def __del__(self):
        release_exe_memory(self._address)

    def code_address(self) -> int:
        return align(self._address, 64)

    def data_address(self) -> int:
        return align(self._address, 64) + align(self._size, 65536)

    def load_code(self, code: bytes):
        if len(code) > self._size:
            raise ValueError("Capacity is %s and code is %i" %
                             (self._size, len(code)))
        ctype_obj = c_char_p(code)
        memmove(self.code_address(), ctype_obj, len(code))
        func = PYFUNCTYPE(None)
        self._func_pointer = func(self.code_address())

    def run(self):
        self._func_pointer()


class MultiExecutableMemory:
    def __init__(self, size: int, data_size: int = 0, nthreads=4):
        self._size = size
        self._data_size = data_size
        self._nthreads = nthreads
        csize = align(self._size + 64, 65536) * nthreads
        dsize = align(data_size, 65536) * nthreads
        self._address = alocate_exe_memory(csize + dsize)

    def __del__(self):
        release_exe_memory(self._address)

    def code_address(self):
        csize = align(self._size, 65536)
        start = align(self._address, 64)
        return [start + n * csize for n in range(self._nthreads)]

    def data_address(self):
        csize = align(self._size, 65536)
        dsize = align(self._data_size, 65536)
        start = align(self._address, 64) + self._nthreads * csize
        return [start + n * dsize for n in range(self._nthreads)]

    def load_code(self, codes):
        if len(codes) != self._nthreads:
            raise ValueError("Expected %i executable codes." % self._nthreads)

        for code, address in zip(codes, self.code_address()):
            ctype_obj = c_char_p(code)
            memmove(address, ctype_obj, len(code))

    def run(self):
        handles = []
        for func_addr in self.code_address():
            handles.append(create_thread(func_addr))
        wait_threads(handles)
