
def align(value: int, align_value: int) -> int:
    return (value + align_value - 1) & ~(align_value - 1)
