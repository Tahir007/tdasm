
from tdasm import MultiTdasmJit, FuncJit


def test_simple_multi_jit():
    asm = """
#DATA
int32 x
int32 y
#CODE
mov eax, dword[x]
mov dword [y], eax
"""

    mjit = MultiTdasmJit(asm, nthreads=4)
    mjit.set_param('x', 4)  # set 4 to all threads
    mjit.set_param('x', 6, thread_idx=2)
    mjit.run()
    result = mjit.get_param('y')
    assert result[0] == 4
    assert result[1] == 4
    assert result[2] == 6
    assert result[3] == 4


def test_multi_func_jit():
    func_asm = """
    #CODE
    mov eax, 5
    ret
    """
    fun = FuncJit(func_asm, 'calc_val')

    asm = """
#DATA
int32 x
#CODE
call calc_val
mov dword [x], eax
"""
    mjit = MultiTdasmJit(asm, nthreads=4, jit_funcs=[fun])
    mjit.run()
    result = mjit.get_param('x')
    assert result[0] == 5
    assert result[1] == 5
    assert result[2] == 5
    assert result[3] == 5
