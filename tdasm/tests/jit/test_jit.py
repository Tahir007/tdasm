
from tdasm import TdasmJit, FuncJit


def test_simple_jit():
    asm = """
#DATA
int32 x
#CODE
mov dword [x], 32
"""
    jit = TdasmJit(asm)
    jit.run()
    result = jit.get_param('x')
    assert result == 32


def test_func_jit():
    func_asm = """
    #CODE
    mov eax, 5
    ret
    """
    fun = FuncJit(func_asm, 'calc_val')

    asm = """
#DATA
int32 x
#CODE
call calc_val
mov dword [x], eax
"""
    jit = TdasmJit(asm, jit_funcs=[fun])
    jit.run()
    result = jit.get_param('x')
    assert result == 5
