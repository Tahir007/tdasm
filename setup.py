
import platform
from setuptools import setup, Extension


if platform.system() == "Windows":
    excmem = Extension('x86', ['x86/x86/main.cpp'],
                       define_macros=[('MICROSOFT_WINDOWS', 1)])
else:
    extra_compile_args = ['-std=c++11']
    excmem = Extension('x86', ['x86/x86/main.cpp'], extra_compile_args=extra_compile_args)

setup(name='tdasm',
      version='0.6.0',
      description='Tdasm is dynamic X86 assembler for python',
      author='Mario Vidov',
      author_email='mvidov@yahoo.com',
      url='https://bitbucket.org/Tahir007/tdasm',
      license='MIT',
      classifiers=['Intended Audience :: Developers',
                   'Programming Language :: Assembly',
                   'Topic :: Software Development :: Assemblers',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   ],

      keywords='assembler simd SSE AVX FMA AVX-512',
      packages=['tdasm'],
      package_dir={'tdasm': 'tdasm'},
      ext_modules=[excmem]
      )
